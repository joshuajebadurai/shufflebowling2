﻿using UnityEngine;
using System.Collections;

public class ParticleEffects : MonoBehaviour {


	public GameObject explosion;

	private GameObject fx;


	public enum FXType{
		EXPLOSION,
		SPARKLES,
		FIREWORKS,
		CONFETTI
	}

	// Use this for initialization
	void Start () {
		//PlayParticleEffect ("CFXM_Explosion", FXType.EXPLOSION, new Vector3(0f,2f,0f));
	}

	public void PlayParticleEffect(string fxName, FXType fxType, Vector3 position){

		fx = Instantiate (Resources.Load<GameObject>(GetFXPath(fxType) + fxName)) as GameObject;
		fx.transform.parent = this.gameObject.transform;

		if (position.x != 0 || position.y != 0f || position.z != 0f) {
			fx.transform.position = position;
		}
	}

	string GetFXPath(FXType type){
		string path;
		if (type == FXType.EXPLOSION)
			path = "FX Pack/Prefabs/Explosions/";
		else
		if (type == FXType.SPARKLES)
			path = "FX Pack/Prefabs/Sparks/";
		else
			if(type == FXType.FIREWORKS)
				path = "FX Pack/Prefabs/Fireworks/";
		else
			path = "FX Pack/Prefabs/Confetti/";

		return path;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
