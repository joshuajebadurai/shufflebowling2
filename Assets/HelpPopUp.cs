﻿using UnityEngine;
using System.Collections;

public class HelpPopUp : MonoBehaviour {

	public Texture2D bg;
	public Texture2D text;
	public Texture2D ok;

	public Rect okButtonRect;
	public Rect textRect;

	public bool isShown = false;

	public GUIStyle okStyle;
	// Use this for initialization
	void Start () {
		okButtonRect = new Rect (Screen.width/2 - ok.width/2, 400 , ok.width,ok.height);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		GUI.depth = 0;
		if (isShown) {
			GUI.DrawTexture (new Rect (Screen.width / 2 - bg.width / 2, Screen.height / 2 - bg.height / 2, bg.width, bg.height), bg);
			GUI.DrawTexture (new Rect (Screen.width / 2 - text.width / 2, Screen.height / 2 - text.height / 2, text.width, text.height), text);
			if (GUI.Button (okButtonRect, "", okStyle)) {
				isShown = false;
			}
		}
	}
}
