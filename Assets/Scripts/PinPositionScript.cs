﻿using UnityEngine;
using System.Collections;

public class PinPositionScript : MonoBehaviour {


	public Vector3 groupPosition;

	public Texture2D pin1Green;
	public Texture2D pin2Green;
	public Texture2D pin3Green;
	public Texture2D pin4Green;
	public Texture2D pin5Green;
	public Texture2D pin6Green;
	public Texture2D pin7Green;
	public Texture2D pin8Green;
	public Texture2D pin9Green;
	public Texture2D pin10Green;

	public Texture2D pin1Blue;
	public Texture2D pin2Blue;
	public Texture2D pin3Blue;
	public Texture2D pin4Blue;
	public Texture2D pin5Blue;
	public Texture2D pin6Blue;
	public Texture2D pin7Blue;
	public Texture2D pin8Blue;
	public Texture2D pin9Blue;
	public Texture2D pin10Blue;

	public Rect pin1Rect;
	public Rect pin2Rect;
	public Rect pin3Rect;
	public Rect pin4Rect;
	public Rect pin5Rect;
	public Rect pin6Rect;
	public Rect pin7Rect;
	public Rect pin8Rect;
	public Rect pin9Rect;
	public Rect pin10Rect;

	public bool isLocked;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetPinsTextures(string[] pins){
		foreach (string s in pins) {
			switch(s){
			case "0":
				pin1Blue = pin1Green;
				break;
			case "1":
				pin2Blue = pin2Green;
				break;
			case "2":
				pin3Blue = pin3Green;
				break;
			case "3":
				pin4Blue = pin4Green;
				break;
			case "4":
				pin5Blue = pin5Green;
				break;
			case "5":
				pin6Blue = pin6Green;
				break;
			case "6":
				pin7Blue = pin7Green;
				break;
			case "7":
				pin8Blue= pin8Green;
				break;
			case "8":
				pin9Blue = pin9Green;
				break;
			case "9":
				pin10Blue = pin10Green;
				break;
			}
		}

		pin1Rect = new Rect (60, 100, pin1Green.width, pin1Green.height);
		pin2Rect = new Rect (85, 70, pin2Green.width, pin2Green.height);
		pin3Rect = new Rect (35, 70, pin3Green.width, pin3Green.height);
		pin4Rect = new Rect (110, 40, pin4Green.width, pin4Green.height);
		pin5Rect = new Rect (61, 40, pin5Green.width, pin5Green.height);
		pin6Rect = new Rect (15, 40, pin6Green.width, pin6Green.height);
		pin7Rect = new Rect (135, 5, pin7Green.width, pin7Green.height);
		pin8Rect = new Rect (85, 5, pin8Green.width, pin8Green.height);
		pin9Rect = new Rect (40, 5, pin9Green.width, pin9Green.height);
		pin10Rect = new Rect (0, 5, pin10Green.width, pin10Green.height);
	}

	void OnGUI(){
		if (!isLocked) {
			GUI.depth = 0;
			GUI.BeginGroup (new Rect (groupPosition.x, groupPosition.y, 400, 400));
			GUI.DrawTexture (pin1Rect, pin1Blue);
			GUI.DrawTexture (pin2Rect, pin2Blue);
			GUI.DrawTexture (pin3Rect, pin3Blue);
			GUI.DrawTexture (pin4Rect, pin4Blue);
			GUI.DrawTexture (pin5Rect, pin5Blue);
			GUI.DrawTexture (pin6Rect, pin6Blue);
			GUI.DrawTexture (pin7Rect, pin7Blue);
			GUI.DrawTexture (pin8Rect, pin8Blue);
			GUI.DrawTexture (pin9Rect, pin9Blue);
			GUI.DrawTexture (pin10Rect, pin10Blue);
			GUI.EndGroup ();
		}
	}
}
