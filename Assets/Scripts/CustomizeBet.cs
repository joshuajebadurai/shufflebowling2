using UnityEngine;
using System.Collections;

public class CustomizeBet : MonoBehaviour {

	public AudioClip menuOn;
	public AudioClip menuOff;
	public AudioClip buttonClip;
	public AudioClip setStakesClip;

	public GameObject customDeno;
	public Texture2D playButton;
	public Texture2D textBox;

	public Texture2D customStakes;

	public GameObject deno1;
	public GameObject deno2;
	public GameObject deno3;


	public Rect playButtonRect;
	public Rect textBoxRect;
	
	public Rect customStakesRect;
	
	public Rect deno1Rect;
	public Rect deno2Rect;
	public Rect deno3Rect;


	public GUIStyle labelStyle;
	public GUIStyle playStyle;

	// Use this for initialization
	void Start () {

		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(menuOn);

		audio.volume = Properties.voiceOverVolume;
		audio.PlayOneShot (setStakesClip);

		Properties.PreviousScene = "ChallengesScene";
		Properties.CurrentScene = "CustomizeBetScene";

		playButtonRect = new Rect (Screen.width/2 - playButton.width/2, 480,playButton.width,playButton.height);
		textBoxRect = new Rect (Screen.width/2 - textBox.width/2, 370,textBox.width,textBox.height);
		customStakesRect = new Rect (Screen.width/2 - customStakes.width/2, 170,customStakes.width,customStakes.height);

		deno1 = GameObject.Instantiate (customDeno, new Vector3 (275,215,0), Quaternion.identity) as GameObject; 
		deno1.GetComponent<CustomDeno> ().value = 1;

		deno2 = GameObject.Instantiate (customDeno, new Vector3 (450,215,0), Quaternion.identity) as GameObject; 
		deno2.GetComponent<CustomDeno> ().value = 50;

		deno3 = GameObject.Instantiate (customDeno, new Vector3 (625,215,0), Quaternion.identity) as GameObject; 
		deno3.GetComponent<CustomDeno> ().value = 25;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	int count = 0;

	IEnumerator PlayAudio(AudioClip audioClip, string buttonName){
		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(audioClip);
		yield return new WaitForSeconds (audioClip.length );
		
		audio.PlayOneShot(menuOff);
		yield return new WaitForSeconds (menuOff.length );
		
		switch (buttonName) {
		case "Play":
			Properties.S_GoingtoPlay = true;
			Properties.GamePlayBalance -= Properties.SelectedDenomination;
			WorkAround.LoadLevelWorkaround("MainGame");
			break;
		}
	}

	void OnGUI(){

		GUI.DrawTexture (customStakesRect, customStakes);
		GUI.DrawTexture (textBoxRect, textBox);

		GUI.BeginGroup (textBoxRect);
		{
			var sizeOfLabel = labelStyle.CalcSize(new GUIContent(string.Format("{0:N2}", Properties.SelectedDenomination)));

			GUI.Label(new Rect(textBoxRect.width/2 - sizeOfLabel.x/2, textBoxRect.height/2 - sizeOfLabel.y/2, sizeOfLabel.x, sizeOfLabel.y),string.Format("{0:N2}", Properties.SelectedDenomination),labelStyle);

		}
		GUI.EndGroup ();

		if (GUI.Button (playButtonRect, "",playStyle)) {
			StartCoroutine(PlayAudio(buttonClip, "Play"));
		}
	}

	void OnApplicationQuit() {
		PlayerPrefs.Save();
	}
}
