﻿using UnityEngine;
using System.Collections;

public class ChallengesSettingsIcons : MonoBehaviour {

	public AudioClip menuOn;
	public AudioClip menuOff;

	public AudioClip buttonClip;
	public AudioClip backButtonClip;

	int buttonWidth = 64;
	int buttonHeight = 64;
	int border = 12;

	public GUIStyle optionsStyle;
	public GUIStyle helpStyle;
	public GUIStyle backStyle;
	public GUIStyle textStyle;

	public Rect helpRect;
	public Rect optionsButtonRect;
	public Rect backRect;
	public Rect ticketRect;
	public Rect totalTicketsRect;

	public Texture2D ticketTexture;


	// Use this for initialization
	void Start () {
		backRect = new Rect(border, Screen.height - buttonHeight - border, buttonWidth, buttonHeight);

		helpRect = new Rect (Screen.width - buttonWidth - border, Screen.height - buttonHeight - border, buttonWidth, buttonHeight);

		optionsButtonRect = new Rect (helpRect.x - buttonWidth - border, Screen.height - buttonHeight - border, buttonWidth, buttonHeight);

		ticketRect = new Rect (backRect.xMax + buttonWidth + border + border, Screen.height - buttonHeight - border, ticketTexture.width, ticketTexture.height);

		totalTicketsRect = new Rect (backRect.xMax + border ,Screen.height - buttonHeight - border, ticketTexture.width, ticketTexture.height);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator PlayAudio(AudioClip audioClip, string buttonName){
		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(audioClip);
		yield return new WaitForSeconds (audioClip.length );
		switch (buttonName) {
		case "Help":
			WorkAround.LoadLevelWorkaround("SettingsScene");
			break;
		case "Options":
			WorkAround.LoadLevelWorkaround("Help");
			break;
		case "Back":
			WorkAround.LoadLevelWorkaround(Properties.ParentScene);
			break;
			
		}
	}

	void OnGUI(){
		if (GUI.Button (helpRect, "", helpStyle)) {
			StartCoroutine (PlayAudio (buttonClip, "Help"));
		}

		if (GUI.Button (optionsButtonRect, "", optionsStyle)) {
			StartCoroutine (PlayAudio (buttonClip, "Options"));
		}

		if (GUI.Button (backRect, "", backStyle)) {
			StartCoroutine (PlayAudio (backButtonClip, "Back"));
		}

		var sizeOfLabel = textStyle.CalcSize (new GUIContent (Properties.TotalTickets.ToString()));

		GUI.Label (totalTicketsRect, Properties.TotalTickets.ToString(),textStyle);

		GUI.DrawTexture (new Rect(totalTicketsRect.x + sizeOfLabel.x + border ,Screen.height - buttonHeight - border, ticketTexture.width, ticketTexture.height), ticketTexture);
	}

}
