﻿using UnityEngine;
using System.Collections;

public class GameUI : MonoBehaviour {

	public GUIStyle label;

	public Texture2D rightBg;
	public Texture2D leftBg;

	public Texture2D stakes;
	public Texture2D attempts;

	public Texture2D challengeNumber;
	public Texture2D challengeName;
	public Texture2D challengeDescp;
	public Texture2D pinsTexture;

	public Rect pinsRect;
	public Rect rightBgRect;
	public Rect leftBgRect;
	public Rect stakesRect;
	public Rect attemptsRect;
	public Rect challengeNumberRect;
	public Rect challengeNameRect;
	public Rect challengeDescpRect;
	public Rect attemptsLeftRect;
	public Rect stakesleftRect;

	// Use this for initialization
	void Start () {

//		Properties.musicVolume = 0.3f;
//		PlayerPrefs.SetFloat("MusicVolume",Properties.musicVolume);
//		PlayerPrefs.Save();
//		AudioManager.AudioSource.audio.volume = Properties.musicVolume;

		Properties.CurrentScene = "MainGame";

		rightBgRect = new Rect (Screen.width - rightBg.width,0,rightBg.width, rightBg.height);
		leftBgRect = new Rect (0,0,leftBg.width, leftBg.height);

		pinsRect = new Rect (rightBgRect.width/2 - pinsTexture.width/2, 90, pinsTexture.width, pinsTexture.height);

		stakesRect = new Rect (35, 20, stakes.width, stakes.height);
		attemptsRect = new Rect (180, 20, attempts.width, attempts.height);

		if (Properties.GameType == Properties.Modes.PLAYFORMONEY) {
			int selectedChallengeNumber = Properties.winLoseDictionary[Properties.SelectedChallenge].Index;
			challengeNumber = Resources.Load<Texture2D>("Challenges/Challenge"+(selectedChallengeNumber+1)+"/Challenge");
			challengeName = Resources.Load<Texture2D>("Challenges/Challenge"+(selectedChallengeNumber+1)+"/Name");
			challengeDescp = Resources.Load<Texture2D>("Challenges/Challenge"+(selectedChallengeNumber+1)+"/Description");
			pinsTexture = Resources.Load<Texture2D>("Challenges/Challenge"+(selectedChallengeNumber+1)+"/Numbers");

			challengeNumberRect = new Rect (leftBgRect.width/2 - challengeNumber.width/2, 30,challengeNumber.width, challengeNumber.height);
			challengeNameRect = new Rect (leftBgRect.width/2 - challengeName.width/2, 80,challengeName.width, challengeName.height);
			challengeDescpRect = new Rect (leftBgRect.width/2 - challengeDescp.width/2, 120,challengeDescp.width, challengeDescp.height);

		}
	}
	
	void OnGUI(){
		if (Properties.GameType == Properties.Modes.PLAYFORMONEY) {
			GUI.DrawTexture (rightBgRect, rightBg);
			GUI.DrawTexture (leftBgRect, leftBg);

			GUI.BeginGroup (rightBgRect);
			{
				var rect = new Rect (stakesRect.x, stakesRect.y + 30, stakesRect.width, stakesRect.height);
				Vector2 sizeOfLabel;
				GUI.BeginGroup (rect);
				{
						string stakesString = "$ " + string.Format("{0:N2}", Properties.SelectedDenomination);
						sizeOfLabel = label.CalcSize (new GUIContent (stakesString));
						GUI.Label (new Rect (rect.width / 2 - sizeOfLabel.x / 2, rect.height / 2 - sizeOfLabel.y / 2, sizeOfLabel.x, sizeOfLabel.y), stakesString, label);
				}
				GUI.EndGroup ();
				//TODO adding the attempts from challenges
				var rect1 = new Rect (attemptsRect.x, attemptsRect.y + 30, attemptsRect.width, attemptsRect.height);
				GUI.BeginGroup (rect1);
				{
						string attemptsString = Properties.currentAttempt + " of " + Properties.challengeAttempts;
						sizeOfLabel = label.CalcSize (new GUIContent (attemptsString));
						GUI.Label (new Rect (rect1.width / 2 - sizeOfLabel.x / 2, rect1.height / 2 - sizeOfLabel.y / 2, sizeOfLabel.x, sizeOfLabel.y), attemptsString, label);
				}
				GUI.EndGroup ();

				GUI.DrawTexture (stakesRect, stakes);
				GUI.DrawTexture (attemptsRect, attempts);
				GUI.DrawTexture (pinsRect, pinsTexture);
			}
			GUI.EndGroup ();

			GUI.BeginGroup (leftBgRect);
			{
				GUI.DrawTexture (challengeNameRect, challengeName);
				GUI.DrawTexture (challengeNumberRect, challengeNumber);
				GUI.DrawTexture (challengeDescpRect, challengeDescp);
			}
			GUI.EndGroup ();

		}
	}
}
