using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameScript : MonoBehaviour {
	
	public AudioClip buttonClip;
	public AudioClip pinCollisionClip;
	public AudioClip winClip;
	public AudioClip loseClip;

	public AudioClip[] challengeWinClip = new AudioClip[10];
	public AudioClip[] challengeLoseClips = new AudioClip[3];
	public AudioClip[] strikeCountClips = new AudioClip[10];
	public AudioClip gutterBallClip;
	public AudioClip lastAttemptClip;
	public AudioClip startChallengeClip;
	public GameObject MachinePins;
	public GameObject pinsList;
	public ParticleEffects particleEffects;

	public PinManagerScript pinManagerScript;
	public Puck puck;
	
	int player1Score;
	public int totaltryCount;
	int player2Score;
	int pinsFallen;
	
	int try1Count;
	int try2Count;
	
	Properties.Modes gameMode;

	string challengeMode;
	bool isWinTexure = false;
	int totalStikesCount = 0;
	
	int buttonWidth = 48;
	int buttonHeight = 48;
	
	private static GameScript script;
	
	public static GameScript Get(){
		return script;
	}
	
	//ChallengesCollection challengesCollection;
	public int totalFrames = 0;
	bool showFrameText = false;
	// Use this for initialization
	void Start () {

		Properties.PreviousScene = "MainGame";
//		totalFrames = 8;
	
		script = this;
		gameMode = Properties.GameType;
		challengeMode = Properties.ChallengeMode;
		Properties.GameWonState = "IsPlaying";
		Properties.isPopUpShown = false;
		player1Score = 0;
		totaltryCount = 1;
		player2Score = 0;
		pinsFallen = 0;

		//start frame 1
		pinManagerScript.SetPinsPos ();
		pinManagerScript.CreatePins ();
	
		UpdateMachinePins ();


		List<int> arcadeScoreList = new List<int> ();
		List<int> classicScoreList = new List<int> ();

		//reads the leaderboard
		for (int i = 0; i < 10; i++) {
			if(PlayerPrefs.HasKey("arcadeScore"+i))
				arcadeScoreList.Add(int.Parse(PlayerPrefs.GetString("arcadeScore"+i)));

			if(PlayerPrefs.HasKey("classic"+i))
				classicScoreList.Add(int.Parse(PlayerPrefs.GetString("classicScore"+i)));
		}
		
		if (arcadeScoreList.Count != 0) {
			Properties.arcadeScoresList = arcadeScoreList;
		}

		if (classicScoreList.Count != 0) {
			Properties.classicScoresList = classicScoreList;
		}

		if (Properties.GameType == Properties.Modes.PLAYFORMONEY) {
			audio.volume = Properties.voiceOverVolume;
			audio.PlayOneShot(startChallengeClip);
		}
	}
	
	bool isLoadWonScene = false;
	
	public void YouWin(){
		Properties.isChallengeFinished = true;
		var index = Properties.winLoseDictionary[Properties.ChallengeMode].Index;
		PlayerPrefsX.SetBool(Properties.challengeDictionary[Properties.ChallengesList[index + 1]],false);
		StartCoroutine (PlayAudio(challengeWinClip[index],"Win"));

		switch (index) {
		case 1: PlayParticleIndex (0); break;//Confetti
		case 2: PlayParticleIndex (1); break;//Confetti
		case 3: PlayParticleIndex (3); break;//Sparkles
		case 4: PlayParticleIndex (4); break;//Sparkles
		case 5: PlayParticleIndex (7); break;//Fireworks
		case 6: PlayParticleIndex (9); break;//Fireworks
		case 7: PlayParticleIndex (6); break;//Fireworks
		case 8: PlayParticleIndex (11); break;//Explosion
		case 9: PlayParticleIndex (12); break;//Explosion
		case 10: PlayParticleIndex (13); break;//Explosion
			

		}
		
		//particleEffects.PlayParticleEffect ("Confetti - Diamonds PS", ParticleEffects.FXType.CONFETTI, Vector3.zero);

		//index is the challenge number
		// particle effect triggers here - Challenge 1  (success) to Challenge 10(success) 
		//index 24 - 33 -refer document

		// particle effect triggers here
		//index 21, 22, 23

		// particle effect triggers here
		//index 19,20
	}


	public GameObject[] particleObjects;
	void PlayParticleIndex (int index) {
		if (index < 0 || index > particleObjects.Length) {
			Debug.LogError ("Particle Index not found!");
			return;
		}

		GameObject particleObject = particleObjects[index];

		if (particleObject == null) {
			Debug.LogError ("Particle Object not found!");
			return;
		}

		foreach (ParticleSystem particleSystem in particleObject.GetComponentsInChildren<ParticleSystem>()) {
			particleSystem.Stop();
			particleSystem.Play();
		}
	}
	
	public void YouLose(){
		StartCoroutine (PlayAudio(challengeLoseClips[UnityEngine.Random.Range(0,3)],"Lose"));
	}
	
	public void UpdateTickets(){
		
		ChallengeProperties val = null;
		val = Properties.winLoseDictionary[challengeMode];
		
		if (Properties.GameWonState == "Lose") {
			if(val != null)
			{
				Properties.Tickets = val.LoseAmount;
				Properties.TotalTickets += Properties.Tickets;
			}
		} 
		else {
			if(val!=null){
//				Properties.Tickets = val.WinAmount;
//				Properties.TotalTickets += Properties.Tickets;
			}
		}
		PlayerPrefs.SetInt("Total Tickets", (int)Properties.TotalTickets);
		PlayerPrefs.Save ();
	}

	public void UpdateMachinePins(){

		if (Properties.GameType == Properties.Modes.PLAYFORFUN) {

			if(Properties.PlayMode == Properties.PlayForFunMode.ARCADE)
			{

				string selectedMaterial = "Arcade/green/";
				int index = 1;
				foreach(Transform child in MachinePins.transform)
				{
					child.renderer.material.mainTexture = Resources.Load<Texture2D>(selectedMaterial + index);
					index++;
				}
			}
			else
				{
				string selectedMaterial = "Classic/blue/";

				int index = 1;

				foreach(Transform child in MachinePins.transform)
				{
					child.renderer.material.mainTexture = Resources.Load<Texture2D>(selectedMaterial + index);
					index++;
				}
			}
		}
	}

	public int FindGameObjectInArray(string name, Transform[] elements){
		for (int i = 0; i < elements.Length; i++)
		{
			if (elements[i].name == name)
			{
				return i;
			}
		}
		return -1;
	}

	public bool isUpdate = false;
	
	float timeElapsed = 0f;
	
	IEnumerator CheckHeadPin(string[] stringList){
		bool pinFound = false;
		yield return new WaitForSeconds (0.5f);
		Transform[] childElements = pinsList.transform.GetComponentsInChildren<Transform>();
		foreach (Transform tr in childElements) {
			foreach (string s in stringList) {
				if (tr.name.Contains (s)){
					pinFound = false;
				}
			}
		}
		if (!pinFound)
			YouWin ();
		else
			YouLose ();
	}

	IEnumerator CheckSinglePins(string[] stringList){
		int count = 0;
		Debug.Log ("CheckSinglePins");
		yield return new WaitForSeconds (0.5f);
		Transform[] childElements = pinsList.transform.GetComponentsInChildren<Transform>();
		if (childElements.Length - 1 == 0) 
			YouWin();
		else
			UpdateAttempts ();
	}
	
	IEnumerator CheckBlinkingPins(string[] stringList){
		int count = 0;
		yield return null;//new WaitForSeconds (1f);
		Transform[] childElements = pinsList.transform.GetComponentsInChildren<Transform>();
		foreach(Transform tr in childElements){
			foreach(string s in stringList){
				if(tr.name.Contains(s))
					count++;
			}
		}
		if (count >= 1) 
			YouLose ();
		else			
			YouWin ();
		
	}
	
	IEnumerator ChallengeSceneWait(){
		yield return new WaitForSeconds (1.8f);
		WorkAround.LoadLevelWorkaround("ChallengesScene");
	}
	
	float timeOut = 0;
	
	IEnumerator WinSceneWait(){

		isWinTexure = true;
		Properties.GameWonState = "Win";
		BasicHandShake.GamePlayReveal_flag = true;
		UpdateTickets ();
		yield return new WaitForSeconds (3.5f);
		WorkAround.LoadLevelWorkaround("Results");
	}
	
	void CheckChallenge(string challengeName){

		string[] pinsList;
		pinsList = Properties.winLoseDictionary [Properties.SelectedChallenge].Pins;
		switch (challengeName) {
		case "The Sniper":
		case "Baby Split":
		case "Sleeper":
		case "Bedposts":
		case "Golden Gate":
		case "The Bucket":
		case "Christmas Tree":
			StartCoroutine(CheckSinglePins(pinsList));
			break;
		}
	}
	
	void PuckReleased(){
		if(GameInput.Get ().m_LineCrossed){
			timeOut += Time.deltaTime;
			if (timeOut > 3f && puck.puckVelocity < 0.5f) {
				if(gameMode == Properties.Modes.PLAYFORFUN){
					UpdateScore ();
					UpdatePins ();
					puck.ResetPuck ();
					UpdateFrame ();
					timeOut = 0;
				}
				else{
					//pinManagerScript.ClearPins();
					puck.ResetPuck();
					UpdateAttempts();
				}

				audio.volume = Properties.voiceOverVolume;
				audio.PlayOneShot(gutterBallClip);
			}
		}
	}

	IEnumerator PlayAudio(AudioClip audioClip, string buttonName){

		audio.volume = Properties.voiceOverVolume;
		audio.PlayOneShot(audioClip);

		yield return new WaitForSeconds (audioClip.length );

		switch (buttonName) {

		case "Win":
			StartCoroutine (WinSceneWait());
			break;
			
		case "Lose":
			Properties.GameWonState = "Lose";
			UpdateTickets ();

			WorkAround.LoadLevelWorkaround ("Results");
			break;
		}
	}
	
	public IEnumerator ShowScoreScene(){
		yield return new WaitForSeconds(1f);
		WorkAround.LoadLevelWorkaround("LeaderBoard");
	}

	public void ResetPlayForFun(){
		
		Properties.CurrentScore = Scoring.Get().totalScore.ToString();
		if (Properties.arcadeScoresList.Count > 0) {
			Properties.HighestScore = Properties.arcadeScoresList [0].ToString ();
			if (int.Parse (Properties.CurrentScore) > int.Parse (Properties.HighestScore))
				Properties.isScoreHightest = true;
			else
				Properties.isScoreHightest = false;
		}
		else
			Properties.isScoreHightest = false;
		
		Properties.arcadeScoresList.Add (Scoring.Get().totalScore);
		Properties.arcadeScoresList.Sort();
		Properties.arcadeScoresList.Reverse ();
		
		Properties.HighestScore = Properties.arcadeScoresList [0].ToString();
		
		if (Properties.arcadeScoresList.Count > Properties.LeaderBoardSize) {
			Properties.arcadeScoresList.RemoveRange(Properties.LeaderBoardSize,Properties.arcadeScoresList.Count - Properties.LeaderBoardSize);
		}
		
		for(int i=0;i<Properties.arcadeScoresList.Count;i++)
		{
			PlayerPrefs.SetString("arcadeScore"+i,Properties.arcadeScoresList[i].ToString());
		}
//		for(int i=0;i<Properties.arcadeScoresList.Count;i++)
//		{
//			PlayerPrefs.SetString("arcadeScore"+i,Properties.arcadeScoresList[i].ToString());
//		}
		PlayerPrefs.Save();

		Scoring.Get ().ResetScore ();
		
		StartCoroutine (ShowScoreScene());
		
	}

	bool frameNoShown = true;
	bool audioPinPlayed = false;
	
	public void PlayPinAudio(){
		
		if(!audioPinPlayed)
		{
			audio.volume = Properties.sfxVolume;
			audio.PlayOneShot(pinCollisionClip);
			audioPinPlayed = true;
		}
	}
	
	public void CheckEndOfFrames(){
		if(totalFrames >= (TOTALFRAMES)){
			totalFrames = 0;
			ResetPlayForFun();
		}
	}
	
	int TOTALFRAMES = 10;
	int bonusFrameCount = 0;
	public bool bonusFrame = false;

	void PlayStrikeAudio(int strikeNumber){
		audio.volume = Properties.voiceOverVolume;
		audio.PlayOneShot(strikeCountClips[strikeNumber]);
	}



	void FixedUpdate(){
		switch (gameMode) {
			
		case Properties.Modes.PLAYFORFUN:
			
			CheckEndOfFrames();
			
			if(totalFrames == 0 && frameNoShown){
				Scoring.Get().UpdateFrame(totalFrames, "Frame ");
				frameNoShown = false;
			}
			
			if (puck.IsPuckLaunched) {
				if(puck.isCollided){
					PlayPinAudio();
					timeElapsed +=Time.deltaTime;
					if(timeElapsed >= 3f){
						timeElapsed = 0f;
						puck.isCollided = false;
						UpdateScore();
						UpdatePins();
						puck.ResetPuck ();
						UpdateFrame();
						audioPinPlayed = false;
					}
				}
				else{
					PuckReleased();
				}
			}
			break;
			
		case Properties.Modes.PLAYFORMONEY:

			switch(challengeMode){
			case "Strike !!":
				if(puck.isCollided){
					PlayPinAudio();
					timeElapsed +=Time.deltaTime;
					if(timeElapsed >= 3f){
						timeElapsed = 0f;
						puck.isCollided = false;
						audioPinPlayed = false;
						if(GetTotalPinsDown()==10){
							UpdateFrame();
							PlayStrikeAudio(0);
							YouWin();
							puck.ResetPuck ();
						}
						else{
							pinManagerScript.ClearPins();
							puck.ResetPuck();
							YouLose();
						}
					}
				}
				else{
					PuckReleased();
				}
				break;
				
			case "Spare !":
				if (puck.IsPuckLaunched) {
					if(puck.isCollided){
						PlayPinAudio();
						
						timeElapsed +=Time.deltaTime;
						if(timeElapsed >= 3f){
							timeElapsed = 0f;
							puck.isCollided = false;
							audioPinPlayed = false;
							var pinsDown = GetTotalPinsDown ();
							CalculateFramePoints (pinsDown);
							if(pinsDown == 10 && totaltryCount == 1){
								YouLose();
							}else
							if(pinsDown < 10 && totaltryCount == 1 ){
								UpdatePins();
								puck.ResetPuck ();
								UpdateFrame();
							}
							else
								if(try1Count+try2Count == 10 && totaltryCount == 2)
							{
								YouWin();
							}
							else
								YouLose();
						}
					}
					else{
						PuckReleased();
					}
				}
				break;
				//triple strike
			case "Wild Turkeys":
				if(puck.isCollided){
					PlayPinAudio();
					
					timeElapsed +=Time.deltaTime;
					if(timeElapsed >= 3f){
						timeElapsed = 0f;
						puck.isCollided = false;
						audioPinPlayed = false;
						var pinsDown = GetTotalPinsDown ();
						CalculateFramePoints (pinsDown);

						if(pinsDown < 10){
							UpdateAttempts();
						}
						else{

							PlayStrikeAudio(totalStikesCount);
							totalStikesCount++;
							if(totalStikesCount == 1){
								//TODO particle effect triggers here
								//index 2, 4, 5 refer doc
							}
							
							if(totalStikesCount == 2){
								//TODO particle effect triggers here
								//index 7,8 refer doc
							}

							if(totalStikesCount>=3){
								//TODO particle effect triggers here
								//index 10,11 refer doc
								YouWin();
							}


							ResetPins();
						}
						UpdateFrame();
						puck.ResetPuck();
					}
				}
				else{
					PuckReleased();
				}
				break;
				//Double strike
			case "Double Double":
				if(puck.isCollided){
					PlayPinAudio();
					
					timeElapsed +=Time.deltaTime;
					if(timeElapsed >= 3f){
						timeElapsed = 0f;
						puck.isCollided = false;
						audioPinPlayed = false;
						var pinsDown = GetTotalPinsDown ();
						CalculateFramePoints (pinsDown);
						if(pinsDown <10){
							UpdateAttempts();
						}
						else{
							PlayStrikeAudio(totalStikesCount);
							totalStikesCount++;

							if(totalStikesCount == 1){
								//TODO particle effect triggers here
								//index 2, 4, 5 refer doc
							}

							if(totalStikesCount >= 2){
								YouWin();
							}
							ResetPins();
						}
						
						UpdateFrame();

						puck.ResetPuck();
					}
				}
				else{
					PuckReleased();
				}
				break;
				
			case "Perfect Game":
				if(puck.isCollided){
					PlayPinAudio();
					
					timeElapsed +=Time.deltaTime;
					if(timeElapsed >= 3f){
						timeElapsed = 0f;
						puck.isCollided = false;
						audioPinPlayed = false;
						var pinsDown = GetTotalPinsDown ();
						CalculateFramePoints (pinsDown);
						if(pinsDown <10){
							UpdateAttempts();
						}
						else{

							PlayStrikeAudio(totalStikesCount);
							totalStikesCount++;

							if(totalStikesCount == 1){
								//TODO particle effect triggers here
							   //index 2, 4, 5 refer doc
							}

							if(totalStikesCount == 2){
								//TODO particle effect triggers here
								//index 7,8 refer doc
							}
							if(totalStikesCount == 3){
								//TODO particle effect triggers here
								//index 10,11 refer doc
							}

							if(totalStikesCount > 3 && totalStikesCount< 12){
								//TODO particle effect triggers here
								//index 12 refer doc
							}
							if(totalStikesCount > 4 && totalStikesCount< 11){
								//TODO particle effect triggers here
								//index 13,14,15 refer doc
							}

							if(totalStikesCount >= 12){
								//TODO particle effect triggers here
								//index 16,17,18 refer doc
								YouWin();
							}
							ResetPins();
						}
						
						UpdateFrame();
						puck.ResetPuck();
					}
				}
				else{
					PuckReleased();
				}
				break;
				break;
			case "The Sniper":
			case "Baby Split":
			case "Sleeper":
			case "Bedposts":
			case "Golden Gate":
			case "The Bucket":
			case "Christmas Tree":
				if(puck.isCollided){
					PlayPinAudio();
					timeElapsed +=Time.deltaTime;
					if(timeElapsed >= 3f){
						timeElapsed = 0f;
						puck.isCollided = false;
						audioPinPlayed = false;
						var pinsDown = GetTotalPinsDown ();
						CalculateFramePoints (pinsDown);
						Debug.Log("Before Check Challenge");
						CheckChallenge(challengeMode);
						puck.ResetPuck();
					}
				}
				else{
					PuckReleased();
				}
				break;
			
			}			
			break;
		}
	}
	
	bool isPinBlinking = true;
	
	float timeElapsedForPin = 0;
	// Update is called once per frame
	void Update () {
		
	}
	
	public void UpdatePins(){
		if (totaltryCount == 1 && pinsFallen == 10)
			ResetPins ();
		else
		if (totaltryCount == 2) {
			ResetPins ();
		}
	}

	public void UpdateAttempts(){

		if (Properties.currentAttempt >= Properties.challengeAttempts) {
			YouLose ();
		} 
		else
		{
			Properties.currentAttempt++;
			Scoring.Get().UpdateAttemptTexture();
			totalStikesCount = 0;
			ResetFrame();
			ResetPins();
			if(Properties.currentAttempt == Properties.challengeAttempts){
				audio.volume = Properties.voiceOverVolume;
				audio.PlayOneShot(lastAttemptClip);
			}
		}
	}

	public void ResetFrame(){
		totaltryCount = 1;
	}

	public void UpdateFrame(){
		
		if (totaltryCount >= 2) {
			totalFrames++;

			if(totalFrames == (TOTALFRAMES-1) && bonusFrame) 
				Scoring.Get().UpdateFrame(bonusFrameCount, "Bonus Frame ");	
			else
				if(totalFrames < TOTALFRAMES)
					Scoring.Get().UpdateFrame(totalFrames, "Frame ");
			
			totaltryCount = 1;
			
		} else
			totaltryCount++;
	}
	
	public void UpdateScore(){
		
		var pinsDown = GetTotalPinsDown ();

		CalculateFramePoints (pinsDown);

		switch (totaltryCount) {
		case 1:
			if(pinsFallen == 10)
			{
				try1Count = 0;
				UpdateScore(pinsDown, totaltryCount, (int)Scoring.HitType.STRIKE);
				totaltryCount = 2;

				if(!bonusFrame){
					if(totalFrames == TOTALFRAMES - 1){
						TOTALFRAMES++;
						bonusFrameCount++;
						bonusFrame = true;
					}
				}
				else{
					if(bonusFrameCount>0){
						try1Count = 0;
						try2Count = 0;
						TOTALFRAMES++;
						totalFrames++;
						totaltryCount = 1;
					}
				}

			}
			else
			{
				UpdateScore(pinsDown, totaltryCount, (int) Scoring.HitType.HIT);
			}
			break;
		case 2:
			if(try1Count+try2Count >= 10){
				UpdateScore(pinsDown, totaltryCount, (int)Scoring.HitType.SPARE);
				if(totalFrames == TOTALFRAMES - 1 && !bonusFrame){
					bonusFrame = true;
					try1Count = 0;
					try2Count = 0;
					TOTALFRAMES++;
					totalFrames++;
					totaltryCount = 1;
					Scoring.Get().UpdateFrame(bonusFrameCount, "Bonus Roll ");	
					ResetPins();
				}
			}
			else
			{
				UpdateScore(pinsDown, totaltryCount, (int)Scoring.HitType.HIT);
			}
			break;
		}
		
		
	}
	
	public void ResetPins(){
		UpdateMachinePins ();
		pinManagerScript.ResetFrame ();
	}

	public void UpdateMachinePin(GameObject pin){

		string number = pin.name.Replace ("Pin", "");
		int index = Convert.ToInt32 (number) + 1;
		var childPins = MachinePins.transform.GetComponentsInChildren<Transform>();
		var pinObject = FindGameObjectInArray (index.ToString (), childPins);
		if (pinObject >= 0) 
		{
			string unSelectedMaterial;

			if(Properties.PlayMode == Properties.PlayForFunMode.ARCADE){
				unSelectedMaterial = "Arcade/rose/";

				childPins[index].renderer.material.mainTexture = Resources.Load<Texture2D>(unSelectedMaterial + index);
			}
			else{
				unSelectedMaterial = "Classic/brown/";
				
				childPins[index].renderer.material.mainTexture = Resources.Load<Texture2D>(unSelectedMaterial + index);
			}
		}
	}

	public int GetTotalPinsDown(){
		
		pinsFallen = 0;

		foreach (Transform child in pinsList.transform)
		{
			if(!child.gameObject.activeSelf)
			{
				pinsFallen++;
				UpdateMachinePin(child.gameObject);
				Destroy(child.gameObject);
			}

			var script =  child.GetComponentInChildren<PinScript>();

			if(script != null)
			{
				if(script.IsFallen){
					pinsFallen++;
					UpdateMachinePin(child.gameObject);
					Destroy(child.gameObject);
				}
			}
		}
		return pinsFallen;
	}

	//Calculates the individual pins down in each try of a frame
	public void CalculateFramePoints(int pinsDown){
		
		if (totaltryCount == 1) {
			try1Count = pinsDown;
		}
		else
			if (totaltryCount == 2)
				try2Count = pinsDown;
	}
	
	void UpdateScore(int noOfPinsFall, int frameCount, int hitType){
		Scoring.Get ().UpdateScore (noOfPinsFall, frameCount,totalFrames, hitType);
	}
	
	void OnGUI(){

//		if (isWinTexure )
//		{
//			if(Properties.WonAmount!="0")
//			{
////				Debug.Log(Properties.WonAmount);
//				//GUILayout.BeginArea(new Rect(Screen.width / 2 - 256 / 2, Screen.height / 2 - 256 / 2, 256, 256/* winnerTexture.width, winnerTexture.height*/));
//				GUI.DrawTexture (new Rect (Screen.width / 2 - winnerTexture.width / 2, Screen.height / 2 - winnerTexture.height / 2,  winnerTexture.width, winnerTexture.height), winnerTexture);
//				GUI.Label (wonRect,Properties.WonAmount, labelStyle);
//			}
//			else
//			{
//				Debug.Log(Properties.WonAmount);
//				GUI.DrawTexture (new Rect (Screen.width / 2 - winnerTexture.width / 2, Screen.height / 2 - winnerTexture.height / 2,  winnerTexture.width, winnerTexture.height), winnerTexture_ticket);
//				GUI.Label (wonRect_Tickets,Properties.TotalTickets.ToString(), wonrectStyle);
//			}
//			// GUILayout.EndArea();
//		}


	}
	
	void OnApplicationQuit() {
		PlayerPrefs.Save();
	}
}
