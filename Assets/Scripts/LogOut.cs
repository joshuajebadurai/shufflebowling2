﻿using UnityEngine;
using System.Collections;

public class LogOut : MonoBehaviour {

	public Texture2D yes;
	public Texture2D no;

	public Rect yesRect;
	public Rect noRect;

	public GUIStyle yesStyle;
	public GUIStyle noStyle;

	public AudioClip menuOn;
	public AudioClip menuOff;
	public AudioClip buttonClip;

	// Use this for initialization
	void Start () {
		Properties.PreviousScene = " SettingsScene";

		yesRect = new Rect (320,320,yes.width,yes.height);	
		noRect = new Rect (500,320,no.width,no.height);	
		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(menuOn);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator PlayAudio(AudioClip audioClip, string buttonName){
		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(audioClip);
		yield return new WaitForSeconds (audioClip.length );
		
		audio.PlayOneShot(menuOff);
		yield return new WaitForSeconds (menuOff.length );
		
		switch (buttonName) {
		case "Yes":

			break;

		case "No":
			WorkAround.LoadLevelWorkaround(Properties.PreviousScene);
			break;

		}
		
	}

	void OnGUI(){
		GUI.depth = 0;

		if(GUI.Button(yesRect, "", yesStyle)){
			StartCoroutine(PlayAudio(buttonClip, "Yes"));

		}
		if(GUI.Button(noRect, "", noStyle)){
			StartCoroutine(PlayAudio(buttonClip, "No"));
		}
	}
}
