﻿using UnityEngine;
using System.Collections;

public class Results : MonoBehaviour {

	public AudioClip menuOn;
	public AudioClip menuOff;
	public AudioClip buttonClick;
	public AudioClip wonTicketsClip;

	public GUIStyle textStyle;

	public Texture2D youWin;
	public Texture2D angledTickets;
	public Texture2D tickets;
	public Texture2D okTexture;

	public Rect youWinRect;
	public Rect angledTicketsRect;
	public Rect ticketsRect;
	public Rect okRect;

	public GUIStyle labelStyle;
	public GUIStyle okStyle;

	public Texture2D tryAgain;
	public Texture2D newChallenge;
	public Texture2D challengeCompleted;
	public Texture2D challengeNotCompleted;

	public Rect tryAgainRect;
	public Rect newChallengeRect;
	public Rect challengeCompletedRect;
	public Rect challengeNotCompletedRect;

	// Use this for initialization
	void Start () {

//		Properties.musicVolume = 0.3f;
//		PlayerPrefs.SetFloat("MusicVolume",Properties.musicVolume);
//		PlayerPrefs.Save();
//		AudioManager.AudioSource.audio.volume = Properties.musicVolume;

		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(menuOn);

		okRect = new Rect (Screen.width / 2 - okTexture.width / 2, 480, okTexture.width, okTexture.height);
		youWinRect = new Rect (200, 180,youWin.width,youWin.height);
		angledTicketsRect = new Rect (270, 380, angledTickets.width, angledTickets.height);
		ticketsRect = new Rect (400, 390,tickets.width,tickets.height);

		Properties.PreviousScene = "ChallengesScene";
		Properties.ParentScene = "ChallengesScene";
		tryAgainRect = new Rect (227, 350, tryAgain.width, tryAgain.height);
		newChallengeRect = new Rect (485, 350, newChallenge.width, newChallenge.height);

		challengeCompletedRect = new Rect (Screen.width / 2 - challengeCompleted.width / 2, 250, challengeCompleted.width, challengeCompleted.height);
		challengeNotCompletedRect = new Rect (Screen.width / 2 - challengeNotCompleted.width / 2, 250, challengeNotCompleted.width, challengeNotCompleted.height);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	IEnumerator PlayAudio(AudioClip audioClip, string buttonName, string audioMode){


		if (audioMode != "VoiceOver") {

			audio.volume = Properties.sfxVolume;
			audio.PlayOneShot (audioClip);

			audio.PlayOneShot (menuOff);
			yield return new WaitForSeconds (menuOff.length);
			} 
		else {
			audio.volume = Properties.voiceOverVolume;
			audio.PlayOneShot (audioClip);

			}

//		PlayAudio (audioClip, "Back");
//		yield return new WaitForSeconds (audioClip.length );

		switch(buttonName){
		case "Ok":
			WorkAround.LoadLevelWorkaround("ChallengesScene");
			break;
		}
	}

	void OnGUI(){

		GUI.DrawTexture (youWinRect, youWin);
		GUI.DrawTexture (ticketsRect, tickets);
		GUI.DrawTexture (angledTicketsRect, angledTickets);

		if (GUI.Button (okRect,"",okStyle)) {
			Properties.challengeNumber = Properties.winLoseDictionary[Properties.SelectedChallenge].Index + 1;
			Properties.challengeDirection = true;
			StartCoroutine(PlayAudio(buttonClick,"Ok", ""));
		}

		if (Properties.isChallengeFinished){
			if(Properties.WonAmount != "0") 
			{
				var winAmount = Properties.WonAmount;
				var sizeOflabel = labelStyle.CalcSize (new GUIContent ("$ " + winAmount.ToString ()));
				GUI.Label (new Rect (Screen.width / 2 - sizeOflabel.x / 2, Screen.height / 2 - sizeOflabel.y / 2, sizeOflabel.x, sizeOflabel.y), "$ " + winAmount.ToString (), labelStyle);
			} else {
				StartCoroutine(PlayAudio(wonTicketsClip,"","VoiceOver"));
			}
		}else {
			StartCoroutine(PlayAudio(wonTicketsClip,"","VoiceOver"));
		}
//		if (Properties.isChallengeFinished)
//			GUI.DrawTexture (challengeCompletedRect, challengeCompleted);
//		else
//			GUI.DrawTexture (challengeNotCompletedRect, challengeNotCompleted);
//
//		if (GUI.Button (tryAgainRect, tryAgain, textStyle)) {
//			Properties.isChallengeFinished = false;
//			Properties.challengeAttempts = Properties.winLoseDictionary [Properties.ChallengeMode].Attempts;
//			WorkAround.LoadLevelWorkaround ("MainGame");
//		}
//
//		if (GUI.Button (newChallengeRect, newChallenge, textStyle)) {
//			WorkAround.LoadLevelWorkaround ("ChallengesScene");
//		}
	}
}
