using UnityEngine;
using System.Collections;
using System.IO;

public class UnLockChallenge : MonoBehaviour {

	public AudioClip buttonClip;
	public AudioClip menuOn;
	public AudioClip menuOff;
	public GUIStyle labelStyle;
	public GUIStyle yesStyle;
	public GUIStyle noStyle;

	public Texture2D yesTexture;
	public Texture2D noTexture;


	public Texture2D challengeBlackBG;
	public Texture2D unlockedTextTexture;
	public Texture2D textTexture;
	public Texture2D lockTexure;
	public Texture2D unLockTexure;


	public Rect yesTextureRect;
	public Rect noTextureRect;

	public Rect challengeBlackBGRect;
	public Rect textTextureRect;
	public Rect lockTexureRect;
	public Rect labelRect;

	Vector2 sizeOfLabel1;

	string labelString;
	// Use this for initialization
	void Start () {

		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(menuOn);

		Properties.PreviousScene = "UnLockChallenge";
		yesTextureRect = new Rect (50, 180, yesTexture.width, yesTexture.height);

		noTextureRect = new Rect (300, 180, noTexture.width, noTexture.height);

		challengeBlackBGRect = new Rect (Screen.width / 2 - challengeBlackBG.width / 2, Screen.height / 2 - challengeBlackBG.height / 2 + 35, challengeBlackBG.width, challengeBlackBG.height);
		
		lockTexureRect = new Rect (challengeBlackBGRect.center.x - lockTexure.width / 2, challengeBlackBGRect.center.y - lockTexure.height / 2, lockTexure.width, lockTexure.height);

//		texture = challengeLockedTexture;
//
//		unlockChallengeRect = new Rect (Screen.width/2 - unlockChallenge.width/2,345,unlockChallenge.width,unlockChallenge.height);
//
//		playAnotherChallengeRect = new Rect (Screen.width/2 - playAnotherChallenge.width/2,445,playAnotherChallenge.width,playAnotherChallenge.height);
//		
//		challengeLockedRect = new Rect (Screen.width/2 - challengeLockedTitle.width/2,220,challengeLockedTitle.width,challengeLockedTitle.height);
//		challengeLockedTextureRect = new Rect (307,125,challengeLockedTexture.width,challengeLockedTexture.height);
//		challengeRect = new Rect (Screen.width/2 - challengeTitle.width/2,80,challengeTitle.width,challengeTitle.height);
//
//		optionsButtonRect = new Rect (0, 0, buttonWidth, buttonHeight);
//		helpRect = new Rect (Screen.width - buttonWidth,0, buttonWidth, buttonHeight);
//
//		gameplayBalanceRect = new Rect(112, 568, 0, 0);
//		ticketsRect = new Rect (858,567,0,0);
//
//		okButtonRect = new Rect (360, 335, buttonBG.width/2, buttonBG.height/2);
//		cancelButtonRect = new Rect (530, 335, buttonBG.width/2, buttonBG.height/2);
//
//		challengeUnlockedIconRect =  new Rect(Screen.width/2 - challengeUnlockedIcon.width/2,250, challengeUnlockedIcon.width, challengeUnlockedIcon.height);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnApplicationQuit() {
		PlayerPrefs.Save();
	}

	IEnumerator PlayAudio(AudioClip audioClip, string buttonName){
		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(audioClip);
		yield return new WaitForSeconds (audioClip.length );

		audio.PlayOneShot(menuOff);
		yield return new WaitForSeconds (menuOff.length );

		switch (buttonName) {
		case "YesButton":
			var challengeProperties = Properties.winLoseDictionary[Properties.ChallengeMode];
			int unLockAmount = challengeProperties.UnLockAmount; 

			PlayerPrefsX.SetBool(Properties.winLoseDictionary[Properties.ChallengeMode].Name,false);
			
			Properties.TotalTickets -= unLockAmount;
			
			PlayerPrefs.SetInt("Total Tickets", (int)Properties.TotalTickets);
			
			PlayerPrefs.Save();

			challengeUnlocked = true;

			WorkAround.LoadLevelWorkaround("UnLockedChallenge");
			break;

		case "NoButton":
			WorkAround.LoadLevelWorkaround("ChallengesScene");
			break;

//		case "Help":
//			Properties.PreviousScene = "UnLockChallenge";
//			WorkAround.LoadLevelWorkaround("Help");
//			break;
//		case "Options":
//			Properties.PreviousScene = "UnLockChallenge";
		//			WorkAround.LoadLevelWorkaround("OptionschallengeBlackBGRect");
//			break;
//		case "PlayAnotherChallenge":
//			WorkAround.LoadLevelWorkaround ("ChallengesScene");
//			break;
//		case "UnLockChallenge":
//			var challengeProperties = Properties.winLoseDictionary[Properties.ChallengeMode];
//			int unLockAmount = challengeProperties.UnLockAmount; 
//			if(unLockAmount	<= Properties.TotalTickets){
//				
//				PlayerPrefsX.SetBool(Properties.winLoseDictionary[Properties.ChallengeMode].Name,false);
//				
//				Properties.TotalTickets -= unLockAmount;
//				
//				PlayerPrefs.SetInt("Total Tickets", (int)Properties.TotalTickets);
//				
//				PlayerPrefs.Save();
//				//show unlockedScreen
//				texture = challengeUnLockedTexture;
//				challengeUnlocked = true;
//				StartCoroutine("ChangeScreen");
////					WorkAround.LoadLevelWorkaround("UnLockedChallenge");
//			}
//			
//			else{
//				Properties.MoreTickets = unLockAmount - (int)Properties.TotalTickets;
//				WorkAround.LoadLevelWorkaround("MoreTickets");
//			}
//			break;
		}
	}

	IEnumerator ChangeScreen(){
		yield return new WaitForSeconds (3f);
		WorkAround.LoadLevelWorkaround ("ChallengesScene");
	}

	bool challengeUnlocked = false;
	bool isTransitionStarted = false;
	void OnGUI(){

		GUI.depth = 0;

		if (!challengeUnlocked) 
		{
			GUI.BeginGroup (challengeBlackBGRect);

			if (GUI.Button (yesTextureRect, "", yesStyle)) {
				StartCoroutine (PlayAudio (buttonClip, "YesButton"));
			}
	
			if (GUI.Button (noTextureRect, "", noStyle)) {
				StartCoroutine (PlayAudio (buttonClip, "NoButton"));
			}
			//Properties.winLoseDictionary[Properties.ChallengeMode].UnLockAmount 
			labelString = "ARE YOU SURE YOU WANT TO SPEND " + Properties.winLoseDictionary [Properties.ChallengeMode].UnLockAmount + "B TICKETS TO UNLOCK THIS CHALLENGE";
			sizeOfLabel1 = labelStyle.CalcSize (new GUIContent (labelString));
	
			GUI.Label (new Rect (0, -60, challengeBlackBGRect.width, challengeBlackBGRect.height), labelString, labelStyle);

			GUI.EndGroup ();
		}
		else
		{
			GUI.BeginGroup (challengeBlackBGRect);
			GUI.DrawTexture(new Rect(challengeBlackBGRect.width/2 - unLockTexure.width/2, challengeBlackBGRect.height/2 - unLockTexure.height/2 , unLockTexure.width, unLockTexure.height), unLockTexure);
			GUI.DrawTexture(new Rect(challengeBlackBGRect.width/2 - unlockedTextTexture.width/2, challengeBlackBGRect.height/2 - unlockedTextTexture.height/2 , unlockedTextTexture.width, unlockedTextTexture.height), unlockedTextTexture);
			GUI.EndGroup();

			if(!isTransitionStarted){
				StartCoroutine(ChangeScreen());
				isTransitionStarted = true;
			}

		}
	}
}
