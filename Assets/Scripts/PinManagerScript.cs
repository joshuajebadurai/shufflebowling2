using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PinManagerScript : MonoBehaviour {

	private static PinManagerScript script;
	public static PinManagerScript Get(){
		return script;
	}

	public GameObject pinsPosTriangle;
	public List<Vector3> pinsPos = new List<Vector3>();

	public GameObject pin;

	// Use this for initialization
	void Start () {
		script = this;
	}

	public void SetPinsPos(){
		var count = 1;
		//Getting the pins position into a list
		while (count!=11) {
			var ob = pinsPosTriangle.transform.FindChild("pin_"+count+"_pos");
			count++;
			pinsPos.Add(ob.position);
		}
	}
	// Update is called once per frame
	void Update () {
	
	}

	public void CreatePins(){
		if (Properties.GameType == Properties.Modes.PLAYFORFUN) {

			GameObject pinsListGameObect = GameObject.Find ("Pins");
			for(int i = 0; i < pinsPos.Count; i++){
				var pinObject = Instantiate(pin, pinsPos[i],pin.transform.rotation)as GameObject;
				pinObject.name = "Pin"+i;
				pinObject.transform.parent = pinsListGameObect.transform;
			}

		} else {

			string[] pins = Properties.winLoseDictionary[Properties.ChallengeMode].Pins;
			GameObject pinsListGameObect = GameObject.Find ("Pins");

			foreach(string s in pins){
				int val = Convert.ToInt32(s);
				var pinObject = Instantiate(pin, pinsPos[val],pin.transform.rotation)as GameObject;
				pinObject.name = "Pin"+ val;
				pinObject.transform.parent = pinsListGameObect.transform;

			}
		}

	}

	/// <summary>
	/// Removes the pins which were pinned down
	/// </summary>
	public IEnumerator RemovePins(){
		yield return new WaitForSeconds (1f);
		CreatePins ();
	}

	int count = 0;
	public Texture2D pinTexture_white;
	public Texture2D pinTexture_gold;
	public Texture2D pinTexture_bronze;
	public Texture2D pinTexture_rainbow;

	List<GameObject> previousPinList = new List<GameObject>();

	public string[] GetBlinkPinsList(){
		var pinNamesList = new List<string>();
		foreach (GameObject g in previousPinList)
			pinNamesList.Add (g.name);

		return pinNamesList.ToArray();
	}

	public void ChangeMaterials(){
		switch (Properties.ChallengeMode) {
		case "Golden Pin":
			if(previousPinList.Count!=0){
				foreach(GameObject p in previousPinList){
					p.gameObject.renderer.material.mainTexture = pinTexture_white;
				}
				previousPinList.Clear();
			}

			var number = Properties.blink1List[count] - 1;
			var pin = GameObject.Find("Pin"+ number);
			pin.gameObject.renderer.material.mainTexture = pinTexture_gold;
			previousPinList.Add(pin);
			count++;
			if(count >= Properties.blink1List.Count)
				count = 0;
			break;
		case "Bronze Pin":
			if(previousPinList.Count!=0){
				foreach(GameObject p in previousPinList){
					p.gameObject.renderer.material.mainTexture = pinTexture_white;
				}
				previousPinList.Clear();
			}

			int[] arrays = Properties.blink2List[count];
			foreach(int i in arrays){
				number = i- 1;
				var pin1 = GameObject.Find("Pin"+ number);
				pin1.gameObject.renderer.material.mainTexture = pinTexture_bronze;
				previousPinList.Add(pin1);
			}
			count++;
			if(count>=Properties.blink2List.Count)
				count = 0;

			break;
		case "Rainbow Pin":

			if(previousPinList.Count!=0){
				foreach(GameObject p in previousPinList){
					p.gameObject.renderer.material.mainTexture = pinTexture_white;
				}
				previousPinList.Clear();
			}
			
			int[] arrayList = Properties.blink3List[count];
			foreach(int i in arrayList){
				number = i- 1;
				var pin1 = GameObject.Find("Pin"+ number);
				pin1.gameObject.renderer.material.mainTexture = pinTexture_rainbow;
				previousPinList.Add(pin1);
			}
			count++;
			if(count>=Properties.blink3List.Count)
				count = 0;
			break;
		}
	}

	public void SevenTenPin(){
		//Getting the pins position into a list
		Transform[] childElements = pinsPosTriangle.transform.GetComponentsInChildren<Transform>();
		foreach(Transform tr in childElements)
		{
			if(tr.name != "Pin_Pos_Triangle" && (tr.name.Contains("pin_10")|| tr.name.Contains("pin_7")))
				pinsPos.Add (tr.position);
		}
	}

	public void ClearPins(){
		Debug.Log ("Clearing Pins");
		GameObject pinsListGameObect = GameObject.Find ("Pins");
		foreach (Transform child in pinsListGameObect.transform)
		{
			Destroy(child.gameObject);
		}
	}
	public void ResetFrame()
	{
		ClearPins ();
		StartCoroutine (RemovePins ());
	}
}
