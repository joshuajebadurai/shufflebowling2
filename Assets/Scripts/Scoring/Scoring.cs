﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scoring : MonoBehaviour 
{

	public ParticleEffects particleEffects;

	public SliderScript spareSliderScript;
	public SliderScript strikeSliderScript;

	public AudioClip scoreClip;
	public AudioClip applauseClip;
	public AudioClip[] spareClips = new AudioClip[3];
	private static Scoring Instance;
	public static Scoring Get()
	{
		return Instance;
	}
	
	void Awake()
	{
		Instance = this;
	}

	public enum HitType{
		HIT,
		SPARE,
		STRIKE
	};

	public struct frameScore
	{
		public int turn1{ get; set; }
		public int turn2{ get; set; }

		public bool isSpare{ get; set; }
		public bool isStrike{ get; set; }
		public bool isHit{ get; set; }

		public int score{ get; set; }
	};

	public int totalScore = 0;

	public TextMesh playerOneScore;
	public TextMesh frameNumber;

	public GameObject text;

	public Texture2D strikeTexture2d;
	public Texture2D spareTexture2d;
	public Texture2D score;

	public Texture2D attemp1;
	public Texture2D attemp2;
	public Texture2D attemp3;
	public Texture2D attemp4;
	public Texture2D attemp5;

	private int m_RoundCount;
	private int	m_FrameCount; 

	GameObject strike;
	GameObject spare;
	GameObject frame;
	GameObject attempts;
	GameObject scoreAnimateObject;

	TextScript strikeScript;
	TextScript spareScript;
	TextScript frameScript;
	TextScript attemptScript;
	TextScript scoreScript;

	public frameScore[] classicModeScoreList = new frameScore[12];

	void Start () 
	{
		m_FrameCount = 0;
		m_RoundCount = 0;

		strike = Instantiate (text, Vector3.zero, Quaternion.identity) as GameObject;
		spare = Instantiate (text, Vector3.zero, Quaternion.identity) as GameObject;
		frame = Instantiate (text, Vector3.zero, Quaternion.identity) as GameObject;
		attempts = Instantiate (text, Vector3.zero, Quaternion.identity) as GameObject;
		scoreAnimateObject = Instantiate (text, Vector3.zero, Quaternion.identity) as GameObject;


		strikeScript = strike.GetComponent<TextScript> ();
		spareScript = spare.GetComponent<TextScript> ();
		frameScript = frame.GetComponent<TextScript> ();
		attemptScript = attempts.GetComponent<TextScript> ();
		scoreScript = scoreAnimateObject.GetComponent<TextScript> ();

		strikeScript.texture2D = strikeTexture2d;
		spareScript.texture2D = spareTexture2d;
		attemptScript.texture2D = attemp1;
		scoreScript.texture2D = score;

		strikeScript.Hide ();
		spareScript.Hide();
		frameScript.Hide ();
		attemptScript.Hide ();
		scoreScript.Hide ();

		playerOneScore.GetComponent<AnimatedTextScript> ().show ();
		frameNumber.GetComponent<AnimatedTextScript> ().show ();

		frameNumber.text = m_FrameCount.ToString ();

		if (Properties.GameType == Properties.Modes.PLAYFORMONEY) {
			Scoring.Get().UpdateAttemptTexture();
		}
	}

	void Update ()
	{

	}


	public void ResetScore (){
		totalScore = 0;
		playerOneScore.text = totalScore.ToString();
		frameNumber.text = m_FrameCount.ToString ();
	}

	public void UpdateFrame(int value, string frameName){

		if (frameName == "Frame ") {
			frameScript.textString = frameName + (value + 1);
			m_FrameCount = value + 1;
			frameNumber.text = m_FrameCount.ToString ();		
		}
		else
			frameScript.textString = frameName; 

		//StartCoroutine(frameScript.ScaleInScaleOut());
	}

	int classicModeScore = 0;
	int TOTALFRAME = 10;

	public void UpdateClassicModeScore(int frameNumber){

		for(int i = 0; i < classicModeScoreList.Length; i++){

			if(i > frameNumber)
				break;

			if(classicModeScoreList[i].isSpare)
			{
				if(i <= frameNumber - 1 )
				{
					classicModeScoreList[i].score = 10;

					if(classicModeScoreList[i + 1].isStrike)
						classicModeScoreList[i].score += 10;
					else
						classicModeScoreList[i].score += classicModeScoreList[i + 1].turn2;

				}
//				else
//
//				if(frameNumber >= TOTALFRAME && GameScript.Get().bonusFrame){
//
//					classicModeScoreList[i].score = 10;
//
//					if(classicModeScoreList[i + 1].isStrike)
//						classicModeScoreList[i].score += 10;
//					else	
//						classicModeScoreList[i].score += classicModeScoreList[i].turn1;
//					}
				}
			else
			if(classicModeScoreList[i].isStrike)
			{
				classicModeScoreList[i].score = 10;

				if(i <= frameNumber - 1)
				{
					if(!classicModeScoreList[i + 1].isStrike)
					{
						if(classicModeScoreList[i + 1].isSpare)
							classicModeScoreList[i].score += 10;
						else
							classicModeScoreList[i].score += classicModeScoreList[i + 1].turn1 + classicModeScoreList[i + 1].turn2;
					}
					else
					if(i <= frameNumber - 2)
					{
						classicModeScoreList[i].score += 10;

						if(classicModeScoreList[i + 2].isStrike)
						{
							classicModeScoreList[i].score += 10;
						
						}else
							classicModeScoreList[i].score += classicModeScoreList[i + 2].turn2;
					}
				}
				else
				if(frameNumber >= TOTALFRAME &&  GameScript.Get().bonusFrame)
				{
					if(classicModeScoreList[i + 1].isStrike)
					{
						if(frameNumber == 12){
							classicModeScoreList[i].score = 20;
							classicModeScoreList[i].score += classicModeScoreList[i + 2].turn1;
						}
					}
					else
					{
						classicModeScoreList[i].score = 10;

						if(classicModeScoreList[i + 1].isSpare)
								classicModeScoreList[i].score += 10;
						else
							classicModeScoreList[i].score += classicModeScoreList[i + 1].turn1 + classicModeScoreList[i + 1].turn2;
					}
				}
			}
			else{
				classicModeScoreList[i].score = classicModeScoreList[i].turn1 + classicModeScoreList[i].turn2;
			}

			Debug.Log("FrameNumber " + (i+1) + " Score :" + classicModeScoreList[i].score);

		}
	}

	public void UpdateAttemptTexture(){

		switch (Properties.currentAttempt) {
		case 1:
			attemptScript.texture2D = attemp1;
			break;
		case 2:
			attemptScript.texture2D = attemp2;
			break;
		case 3:
			attemptScript.texture2D = attemp3;
			break;
		case 4:
			attemptScript.texture2D = attemp4;
			break;
		case 5:
			attemptScript.texture2D = attemp5;
			break;
		}

		StartCoroutine(attemptScript.ScaleInScaleOut());

	}

	void SetScoreTexture (int value){
		scoreScript.texture2D = Resources.Load<Texture2D> ("Scores/" + value.ToString ());
	}

	IEnumerator AnimateScore(HitType hitType, int value){

		SetScoreTexture (value);

		switch (hitType) {
		case HitType.HIT:
			StartCoroutine(scoreScript.ScaleInScaleOut());
			break;
		case HitType.SPARE:
			if(!GameScript.Get().bonusFrame){
				StartCoroutine(spareScript.ScaleInScaleOut());
				yield return new WaitForSeconds(1.2f);
				StartCoroutine(scoreScript.ScaleInScaleOut());
			}
			break;
		case HitType.STRIKE:
			StartCoroutine(strikeScript.ScaleInScaleOut());
			yield return new WaitForSeconds(1.2f);
			StartCoroutine(scoreScript.ScaleInScaleOut());
			break;
		}
	}

	public void UpdateScore (int noOfPinsFall, int frameCount, int frameNumber, int hitType)
	{
		audio.volume = Properties.sfxVolume;

		var sliderValue = 0;

		Debug.Log ("FrameNumber: " + frameNumber + "TryCount: "+ frameCount);

		switch ((HitType)hitType) {
			 
		case HitType.HIT:
			//audio.PlayOneShot (scoreClip);

			playerOneScore.GetComponent<AnimatedTextScript>().zoomIn = true;
		
			if(Properties.PlayMode == Properties.PlayForFunMode.ARCADE){
				totalScore += noOfPinsFall;
				playerOneScore.text = totalScore.ToString();
			}
			else{
				if(frameCount == 1)
					classicModeScoreList[frameNumber].turn1 = noOfPinsFall;
				else
					classicModeScoreList[frameNumber].turn2 = noOfPinsFall;
			}

			StartCoroutine(AnimateScore(HitType.HIT,noOfPinsFall));

			break;

		case HitType.SPARE:
			audio.PlayOneShot (spareClips[Random.Range(0,3)]);
			if(Properties.PlayMode == Properties.PlayForFunMode.ARCADE){
				sliderValue = spareSliderScript.FindCollisionCollider();
				if(sliderValue == 800)
					sliderValue = 500;
				else 
					sliderValue -= 200;
				totalScore+=sliderValue;
				playerOneScore.GetComponent<AnimatedTextScript>().zoomIn = true;
				playerOneScore.text = totalScore.ToString();

				StartCoroutine(AnimateScore(HitType.SPARE, sliderValue));

			}
			else{
				classicModeScoreList[frameNumber].isSpare = true;
			}
			break;
		case HitType.STRIKE:
			audio.PlayOneShot (applauseClip);
			//TODO particle effect triggers here
			//index 2 refer doc

//			particleEffects.PlayParticleEffect("CFXM_Firework", ParticleEffects.FXType.FIREWORKS, new Vector3(0f, 1.5f, -2.5f));
			if(Properties.PlayMode == Properties.PlayForFunMode.ARCADE){
				sliderValue = strikeSliderScript.FindCollisionCollider();
				totalScore+=sliderValue;
				playerOneScore.GetComponent<AnimatedTextScript>().zoomIn = true;
				playerOneScore.text = totalScore.ToString();

				StartCoroutine(AnimateScore(HitType.STRIKE, sliderValue));

			}
			else{
				classicModeScoreList[frameNumber].isStrike = true;
			}
			break;
		}
	
		if (frameCount == 2) {
			UpdateClassicModeScore(frameNumber);
		}
	}
}
