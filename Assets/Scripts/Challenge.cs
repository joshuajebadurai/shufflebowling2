﻿using UnityEngine;
using System.Collections;

public class Challenge : MonoBehaviour
{

	public int index = 0;
	public Texture2D timesTexture;
    public Texture2D challengeBG;
    public Texture2D lockTexture;
    public Texture2D unLockTexture;
	public Texture2D pinPlacementTexture;
	public Texture2D attemptsTexture;
	public Texture2D ticketsAngledTexure;
	public Texture2D lockedTextTexture;
	public Texture2D challengeNameTexture;
	public Texture2D challengeNumberTexture;
	public Texture2D challengeDescTexture;
	public Texture2D pins;

	public Rect timesRect;
	public Rect pinsRect;
	public Rect challengeDescRect;
	public string challengeName = "";
	public string challengeNumber = "";
    public string ticketLabel = "";

    public Rect challengeNumberRect;
    public Rect lockTextureRect;
    public Rect challengeNameRect;
    public Rect challengeRect;
    public Rect challenge1Rect;
	public Rect label1;
	public Rect label2;
	public Rect attemptsTextureRect;
	public Rect ticketsAngledRect;

    public GUIStyle style;
	public GUIStyle unlockStyle;
	public GUIStyle attemptStyle;
	public string name;
    public bool isLocked;

	string label_unlock;
	string label_to_unlock;

	int attempts = 0;
	int unLockTickets;
	PinPositionScript pinPositionScript;
	// Use this for initialization
    void Start()
    {
		//this.transform.position = new Vector3 (245,170,0);
		challengeNumberRect = new Rect(350, 130, challengeNumberTexture.width, challengeNumberTexture.height);
		challengeNameRect = new Rect( challengeBG.width/2 - challengeNameTexture.width/2, 5, challengeNameTexture.width, challengeNameTexture.height);
		challengeDescRect = new Rect (30, 85, challengeDescTexture.width, challengeDescTexture.height);
		pinsRect = new Rect (300, 80, pins.width,pins.height);
		timesRect = new Rect(416, 185, timesTexture.width, timesTexture.height);

		challengeRect = new Rect(0, 0, challengeBG.width, challengeBG.height);

        lockTextureRect = new Rect(210, 32, lockTexture.width, lockTexture.height);
		attemptsTextureRect = new Rect (340, 430, attemptsTexture.width, attemptsTexture.height);
		ticketsAngledRect = new Rect (600, 108, ticketsAngledTexure.width, ticketsAngledTexure.height);
		unLockTickets = Properties.winLoseDictionary[Properties.challengeDictionary[challengeName]].UnLockAmount;
		attempts = Properties.winLoseDictionary [Properties.challengeDictionary [challengeName]].Attempts;
	}

	public GUIStyle challengeBGStyle;

	public void ShowLockedScreen(){

		Properties.ChallengeMode = Properties.challengeDictionary [challengeName];
		
		Properties.SelectedChallenge = name;
		
		if(Properties.TotalTickets >= 	unLockTickets)
		{
			PlayerPrefsX.SetBool(Properties.winLoseDictionary[Properties.ChallengeMode].Name,false);
			
			Properties.TotalTickets -= unLockTickets;
			
			PlayerPrefs.SetInt("Total Tickets", (int)Properties.TotalTickets);
			
			PlayerPrefs.Save();
			
			isLocked = false;
			
			pinPositionScript.isLocked = isLocked;
			
			//WorkAround.LoadLevelWorkaround ("UnLockChallenge");
		}
		else
		{
			WorkAround.LoadLevelWorkaround ("TicketsNeeded");
		}
	}

	public void ShowWagerScreen(){

		Properties.isChallengeFinished = false;
		Properties.ChallengeMode = Properties.challengeDictionary [challengeName];
		Properties.challengeAttempts = Properties.winLoseDictionary[Properties.ChallengeMode].Attempts;
		Properties.currentAttempt = 1;
		Properties.SelectedChallenge = challengeNumber;
		WorkAround.LoadLevelWorkaround ("WagerScene");
	}

    void OnGUI()
    {
		if (!Properties.isPopUpShown) {

			GUI.depth = 1;

			GUI.BeginGroup (new Rect (transform.position.x, transform.position.y, challengeBG.width , challengeBG.height));

			Vector2 sizeOfLabel;

			if(isLocked){

				if(GUI.Button (new Rect (challengeRect.width/2 - lockTextureRect.width/2, challengeRect.height/2 - lockTextureRect.height/2, lockTextureRect.width, lockTextureRect.height), lockTexture, style)){


				}

				GUI.Button(challengeRect, challengeBG, style);

				GUI.DrawTexture(new Rect(challengeBG.width/2 - lockedTextTexture.width/2,challengeBG.height/2-lockedTextTexture.height/2, lockedTextTexture.width, lockedTextTexture.height), lockedTextTexture);		

//				label_unlock = unLockTickets + " b TICKETS TO UNLOCK";
//				sizeOfLabel = style.CalcSize (new GUIContent (label_unlock));
//				label1 = new Rect(0,0,challengeRect.width,challengeRect.height);
//
//				GUI.Label(label1, label_unlock, unlockStyle);
			}
			else
			{
				if(GUI.Button(challengeRect, challengeBG, style)){

				}

				GUI.DrawTexture(challengeNameRect, challengeNameTexture);

				GUI.DrawTexture(challengeDescRect,challengeDescTexture);

				GUI.DrawTexture(pinsRect, pins);

				if(index >= 7){
					GUI.DrawTexture(timesRect, timesTexture);
				}
			}

			GUI.EndGroup ();

			GUI.DrawTexture(challengeNumberRect, challengeNumberTexture);

			GUI.DrawTexture(ticketsAngledRect,ticketsAngledTexure);

			if(!isLocked)
			{
				GUI.DrawTexture(attemptsTextureRect,attemptsTexture);
			
				sizeOfLabel = style.CalcSize (new GUIContent (attempts.ToString()));
				GUI.Label(new Rect( attemptsTextureRect.xMax + 20, attemptsTextureRect.yMax - sizeOfLabel.y, sizeOfLabel.x,sizeOfLabel.y),attempts.ToString(),attemptStyle);
			}
		}
	}
}
