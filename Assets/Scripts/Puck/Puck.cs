﻿using UnityEngine;
using System.Collections;

public class Puck : MonoBehaviour
{
	private static Puck Instance;
	public AudioClip puckReleasedClip;
	public AudioClip wallPuckCollisionClip;
	public GameObject startLine;
	public GameObject endLine;

	public static Puck Get()
	{
		return Instance;
	}

	private Transform m_Transform;
	private Rigidbody m_RigidBody;
	private Vector3	m_DefaultPosition;
	private float m_ForwardSpeed;
	private float m_TimeInEnd;
    public float m_minSpeed=500f;
    public float m_maxSpeed=2000f;
	
	private Vector3	_basePos; //give base position to reset Puck
	private bool _isPuckLaunched;
	public float puckVelocity; //Will help to stop the puck.

    public float m_Velocity; //Speed with which Puck should move after launch.
	private float _launchTime;

	public SliderScript stikeSliderQuad;
	public SliderScript spareSliderQuad;

	public bool IsPuckLaunched {
		get {
			return _isPuckLaunched;
		}
		set {
			_isPuckLaunched = value;
		}
	}

	public bool isCollided = false;
	float timeElapsed = 0f;

	void Awake()
	{
		Instance = this;
		_basePos = transform.position;
		m_Transform = gameObject.transform;
		m_RigidBody = gameObject.GetComponent<Rigidbody> ();
		m_DefaultPosition = gameObject.transform.position;

        //Physics.IgnoreLayerCollision(8, 10);
		//Properties.S_CantakeInputs = true;d
	}
	//private Vector3 fc;
	void FixedUpdate()
	{
		puckVelocity = rigidbody.velocity.magnitude;
	}
    //Vector3 newForce =Vector3.zero;
	public void LaunchPuck(Vector3 force)
	{
        var LaunchForceZ = Mathf.Clamp(force.z * m_Velocity, -m_maxSpeed, -m_minSpeed);
        force = new Vector3(force.x * m_Velocity, force.y, LaunchForceZ);
		rigidbody.AddForce (force , ForceMode.Force);
        //newForce = force;
		IsPuckLaunched = true;
        try
        {
			audio.volume = Properties.sfxVolume;
            audio.PlayOneShot(puckReleasedClip);
        }
        catch (System.Exception)
        {
            throw;
        }
		
		//Set the camera to follow Puck.
		Camera.main.GetComponent<FollowCam>().canFollow = true;
		Camera.main.GetComponent<FollowCam>().zoomOut = false;
	}



	void Update(){
        if (this.rigidbody.position.z < startLine.transform.position.z 
		    && this.rigidbody.position.z > endLine.transform.position.z )
        {
            Properties.S_CantakeInputs = true;
			if(GameInput.Get()!=null)
         	   GameInput.Get().m_LineCrossed = false;
          //  Debug.Log("I am checking for puck position here" + this.rigidbody.position.z);
        }
        else
        {
          //  Debug.Log("Stopping the inputs since puck is not in the input zone" + this.rigidbody.position.z);
            Properties.S_CantakeInputs = false;
			if(GameInput.Get()!=null)
            	GameInput.Get().m_LineCrossed = true;
        }
	}

 	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "Pin") {
			if(!isCollided){
				if(Properties.PlayMode == Properties.PlayForFunMode.ARCADE){
					if(GameScript.Get().totaltryCount == 1){
						stikeSliderQuad.RePosition();
						stikeSliderQuad.Pause();
					}
					else{
						spareSliderQuad.RePosition();
						spareSliderQuad.Pause();
					}
				}
			}
			isCollided = true;
		}
		else
		if (collision.gameObject.tag == "Wall") {
			audio.PlayOneShot(wallPuckCollisionClip);
		}
	}

	void OnTriggerEnter(Collider other) {

	}

	public void ResetPuck()
	{ 

		transform.rigidbody.isKinematic = true;
		transform.position = _basePos;
//		rigidbody.velocity = Vector3.zero; //Zero the velocity

		GameInput.Get().m_LineCrossed = false;
		IsPuckLaunched = false;
        Properties.S_CantakeInputs = true;
		_launchTime = 0f;

		Camera.main.GetComponent<FollowCam> ().zoomOut = true;
		Camera.main.GetComponent<FollowCam>().canFollow = false;

		if(GameScript.Get().totaltryCount == 2)
		{
			StartCoroutine(stikeSliderQuad.Reset());
			StartCoroutine(spareSliderQuad.Reset());
		}
	}
	
	public void RegisterHit(string hitName)
	{
//		switch (hitName)
//		{
//		case "Left":
//
//			m_SideSpeed = -m_SideSpeed;
//
//			break;
//
//		case "Right":
//			
//			m_SideSpeed = -m_SideSpeed;
//			
//			break;
//
//		case "Top":
//			
//			m_ForwardSpeed = -m_ForwardSpeed;
//			
//			break;
//		}
	}

	public Transform GetTransform()
	{
		return m_Transform;
	}
}
