using UnityEngine;
using System.Collections;

public class Pin : MonoBehaviour
{

	public bool					_fallen;
	public Vector3				m_BasePos;

	public bool IsFalen {
		get {
			return _fallen;
		}
		set
		{
			_fallen = value;
		}
	}

	void Start()
	{
		//_fallen = false;
	}

	void Update () 
	{
		if(Vector3.Dot (transform.up, Vector3.up) < 0.99f)
		{
			IsFalen = true;
		}
	}

	void OnCollisionEnter(Collision hit)
	{
		if(hit.gameObject.name.Contains ("End"))
		{
			transform.position = new Vector3(100,100,100);
			transform.rotation = Quaternion.Euler (Vector3.zero);
			rigidbody.isKinematic = true;
			IsFalen = true;

		}
	}
}
