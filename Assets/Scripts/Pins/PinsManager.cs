using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PinsManager : MonoBehaviour 
{
	private static PinsManager	Instance;

	public List<Pin> m_Pins = new List<Pin>(10);

	public List<Vector3> m_PinPos = new List<Vector3>();

	public static PinsManager Get()
	{
		return Instance;
	}

	void Awake()
	{
		Instance = this;
	}

	public int	m_PinCount;

	void Start () 
	{
	}

	public void ShowPins(){
		m_PinCount = 10;
		AddPinPosToList();
		InitPins ();
	}

	void Update()
	{
//		if(m_PinCount <= 0)
//		{
//			//All pins are fallen reset them
//			foreach(Pin p in m_Pins)
//			{
//				p.IsFalen = false;
//			}
//			ResetPinPositions ();
//			m_PinCount = 10;
//		}
	}

	void AddPinPosToList() //String pin base positions.
	{
		Transform pinPosParentTransform = GameObject.Find ("Pin_Pos_Triangle").transform;
		Transform[] childElements = pinPosParentTransform.GetComponentsInChildren<Transform>();
		foreach(Transform tr in childElements)
		{
			if(tr.name != "Pin_Pos_Triangle")
				m_PinPos.Add (tr.position);
		}
	}

	public void InitPins()
	{
		//Initialize Pins
		for(int i = 0; i < 10; i++)
		{
			GameObject pin = Instantiate (Resources.Load ("pin"), m_PinPos[i], Quaternion.identity) as GameObject;
			pin.name = "pin_" + (i + 1);
			pin.GetComponent<Pin>().m_BasePos = m_PinPos[i];
			m_Pins.Add (pin.GetComponent<Pin>());
			pin.transform.parent = GameObject.Find("Pins").transform;
		}
	}

	public void ResetFrame()
	{
//		foreach(Pin p in m_Pins)
//		{
//			p.IsFalen = false;
//		}
//		ResetPinPositions ();
	}

	public void ResetPinPositions()
	{
//		StartCoroutine ("Wait", 6);
//		foreach(Pin p in m_Pins)
//		{
//			if(p.IsFalen == true)
//			{
//				p.transform.position = new Vector3(100,100,100);
//				p.transform.rotation = Quaternion.Euler (Vector3.zero);
//				p.rigidbody.isKinematic = true;
//				m_PinCount--;
//			}
//			else
//			{
//				p.transform.rotation = Quaternion.Euler (Vector3.zero);
//				p.transform.position = p.m_BasePos;
//				p.rigidbody.isKinematic = false;
//			}
//		}
	}

	IEnumerator Wait(float secs)
	{
		yield return new WaitForSeconds(secs);
	}

}
