﻿using UnityEngine;
using System.Collections;

public class Settings : MonoBehaviour {

	public Texture2D backButton;

	public Texture2D help;
	public Texture2D logOut;
	public Texture2D quitGame;
	public Texture2D credits;

	public Rect helpRect;
	public Rect logOutRect;
	public Rect quitGameRect;
	public Rect creditsRect;

	public GUIStyle backStyle;
	public GUIStyle helpStyle;
	public GUIStyle logoutStyle;
	public GUIStyle creditsStyle;
	public GUIStyle quitGameStyle;

	public Rect backRect;

	int buttonWidth = 64;
	int buttonHeight = 64;
	int border = 15;
	public AudioClip buttonClip;
	public AudioClip backClip;

	public AudioClip menuOn;
	public AudioClip menuOff;

	// Use this for initialization
	void Start () {

		Properties.PreviousScene = Properties.CurrentScene;
		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(menuOn);

		backRect = new Rect (border, Screen.height - buttonWidth - border, buttonWidth, buttonHeight);
		helpRect = new Rect (Screen.width/2 - help.width/2, 180, help.width, help.height);
		logOutRect = new Rect (Screen.width/2 - logOut.width/2, 275, logOut.width, logOut.height);
		creditsRect = new Rect (Screen.width/2 - logOut.width/2, 328,credits.width, credits.height);
		quitGameRect = new Rect (Screen.width/2 - quitGame.width/2, 286, quitGame.width, quitGame.height);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator PlayAudio(AudioClip audioClip, string buttonName){
		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(audioClip);
		yield return new WaitForSeconds (audioClip.length );

		audio.PlayOneShot(menuOff);
		yield return new WaitForSeconds (menuOff.length );

		switch (buttonName) {
		case "Back":
			WorkAround.LoadLevelWorkaround(Properties.PreviousScene);
			break;
		case "Help":
			WorkAround.LoadLevelWorkaround("Help");
			break;
		case "LogOut":
				WorkAround.LoadLevelWorkaround("LogOut");
			break;
		case "Quit":
				WorkAround.LoadLevelWorkaround("QuitScene");
			break;
		case "Credits":
//			WorkAround.LoadLevelWorkaround("Credits");
			break;
		}

	}

	void OnGUI(){
		GUI.depth = 0;
		if (GUI.Button(backRect, "", backStyle))
		{
			StartCoroutine(PlayAudio(backClip, "Back"));
		}

//		if (GUI.Button (helpRect, "", helpStyle)) {
////			Properties.PreviousScene = "SettingsScene";
////			Properties.ParentScene = "SettingsScene";
//			StartCoroutine(PlayAudio(buttonClip, "Help"));
//		}
		if (GUI.Button (logOutRect, "", logoutStyle)) {
//			Properties.PreviousScene = "SettingsScene";
//			Properties.ParentScene = "SettingsScene";

			StartCoroutine(PlayAudio(buttonClip, "LogOut"));
		}
//		if (GUI.Button (quitGameRect, "", quitGameStyle)) {
////			Properties.PreviousScene = "SettingsScene";
////			Properties.ParentScene = "SettingsScene";
//
//			StartCoroutine(PlayAudio(buttonClip, "Quit"));
//		}
		if (GUI.Button (creditsRect, "", creditsStyle)) {	
//			Properties.PreviousScene = "SettingsScene";
//			Properties.ParentScene = "SettingsScene";

			StartCoroutine(PlayAudio(buttonClip, "Credits"));
		}

	}
}
