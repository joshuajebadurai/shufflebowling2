﻿using UnityEngine;
using System.Collections;

public class menuScript : MonoBehaviour {

	public Texture2D backGround;
	public Texture2D frame;
	public AudioClip buttonClick;
	public GUIStyle Style;
	public GUIStyle arcadeStyle;
	public GUIStyle classicStyle;

	public Texture2D arcadeModeTex;
	public Texture2D classicMode;

	public Rect arcadeRect;
	public Rect classicRect;

	public AudioClip menuOn;
	public AudioClip menuOff;
	public AudioClip mainButtons;

	// Use this for initialization
	void Start () {

//		Properties.musicVolume = 0.3f;
//		PlayerPrefs.SetFloat("MusicVolume",Properties.musicVolume);
//		PlayerPrefs.Save();
//		AudioManager.AudioSource.audio.volume = Properties.musicVolume;

		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(menuOn);


		Properties.ParentScene = "ModesScene";
		Properties.PreviousScene = "MainMenu";
		Properties.CurrentScene = "MainMenu";
		arcadeRect = new Rect (Screen.width/2 - arcadeModeTex.width/2, 320, arcadeModeTex.width, arcadeModeTex.height);
		classicRect = new Rect (Screen.width/2 - classicMode.width/2, 230, classicMode.width, classicMode.height);
	}

//	TODO Add the Audio functionality
//	IEnumerator PlayAudio(){
//	}

	// Update is called once per frame
	void Update () {

	}

	IEnumerator PlayAudio(AudioClip audioClip, string buttonName){

		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(audioClip);
		yield return new WaitForSeconds (audioClip.length );
		
		audio.PlayOneShot(menuOff);
		yield return new WaitForSeconds (menuOff.length );
		
		switch (buttonName) {
		case "Arcade":
			WorkAround.LoadLevelWorkaround("MainGame");
			break;
		case "Classic":
			WorkAround.LoadLevelWorkaround("MainGame");
			break;

		}
	}

	void OnGUI(){
		GUI.depth = 0;
		if (GUI.Button (arcadeRect, "", arcadeStyle)) {
			Properties.PlayMode = Properties.PlayForFunMode.ARCADE;
			StartCoroutine(PlayAudio(mainButtons, "Arcade"));

		}
		if (GUI.Button (classicRect, "", classicStyle)) {
			Properties.PlayMode = Properties.PlayForFunMode.CLASSIC;
			StartCoroutine(PlayAudio(mainButtons, "Classic"));
		}
	}

	void OnApplicationQuit() {
		PlayerPrefs.Save();
	}
}
