﻿using UnityEngine;
using System.Collections;

public class PinScript : MonoBehaviour {

	public bool IsFallen;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void FixedUpdate () {
		float val = Vector3.Dot(transform.forward, Vector3.up);
         if (val < 0.9f ) {
			IsFallen = true;
		} 
	}

	void OnCollisionEnter(Collision collider){
		if (collider.gameObject.tag == "EndBlock") {
//			IsFallen = true;
//			gameObject.SetActive(false);
			Physics.IgnoreCollision(collider.gameObject.collider, this.gameObject.collider);
		}
	}
}
