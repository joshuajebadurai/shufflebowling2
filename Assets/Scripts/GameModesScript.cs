using UnityEngine;
using System.Collections;

public class GameModesScript : MonoBehaviour {

	public AudioClip menuOn;
	public AudioClip menuOff;
	public AudioClip mainButtons;

    public Texture2D playForFun;
    public Texture2D playForCash;

    public Texture2D BSpot;
	public Texture2D shuffleLogo;

	public Texture2D podium;

	public Rect bSpotRect;

    public Rect playForFunRect;
    public Rect playForCashRect;
	public Rect shufflebowlingRect;
	public Rect podiumRect;
    
	int buttonWidth = 64;
    int buttonHeight = 64;

	public GUIStyle podiumStyle;
	public GUIStyle PlayForFunStyle;
	public GUIStyle PlayForMoneyStyle;


    // Use this for initialization
    void Start () {

//		Properties.musicVolume = 0.3f;
//		PlayerPrefs.SetFloat("MusicVolume",Properties.musicVolume);
//		PlayerPrefs.Save();
//		AudioManager.AudioSource.audio.volume = Properties.musicVolume;

		Properties.ParentScene = "ModesScene";
		Properties.PreviousScene = "ModesScene";
		Properties.CurrentScene = "ModesScene";

		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(menuOff);

		playForFunRect = new Rect (Screen.width/2 - playForFun.width/2, 370, playForFun.width, playForFun.height);
		playForCashRect = new Rect (Screen.width/2 - playForCash.width/2, 450, playForCash.width, playForCash.height);

		podiumRect = new Rect ( 100, 525, buttonWidth, buttonWidth);

		bSpotRect = new Rect (Screen.width/2 - BSpot.width/2, 80, BSpot.width, BSpot.height);
		shufflebowlingRect = new Rect (Screen.width/2-shuffleLogo.width/2, 160, shuffleLogo.width, shuffleLogo.height);
	}
  
	bool isPlayForMoneyClicked = false;

	void Update () {

//		if (isPlayForMoneyClicked) {
//			if (!Properties.IsLoggedIn)
//			{
//				Application.OpenURL(BasicHandShake.loginUrl + "?successUrl=" + BasicHandShake.thisPage + "&apiKey=" + BasicHandShake.apiKey);
//				Debug.Log("testing this: " + (BasicHandShake.loginUrl + "?successUrl=" + BasicHandShake.thisPage + "&apiKey=" + BasicHandShake.apiKey));
//			}
//			else 
//			{
//				WorkAround.LoadLevelWorkaround("ChallengesScene");
//			}
//			isPlayForMoneyClicked = false;
//		}
	}

	IEnumerator PlayAudio(AudioClip audioClip, string buttonName){

		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(audioClip);
		yield return new WaitForSeconds (audioClip.length );

		audio.PlayOneShot(menuOff);
		yield return new WaitForSeconds (menuOff.length );
		
		switch (buttonName) {
		case "PlayForCash":
			isPlayForMoneyClicked = true;
			if (!Properties.IsLoggedIn)
			{
				Application.OpenURL(BasicHandShake.loginUrl + "?successUrl=" + BasicHandShake.thisPage + "&apiKey=" + BasicHandShake.apiKey);
				Debug.Log("testing this: " + (BasicHandShake.loginUrl + "?successUrl=" + BasicHandShake.thisPage + "&apiKey=" + BasicHandShake.apiKey));
			}
			else 
			{
				WorkAround.LoadLevelWorkaround("ChallengesScene");
			}
			break;
		case "PlayForFun":

			WorkAround.LoadLevelWorkaround("MainMenu");
			break;
		case "LeaderBoard":
			WorkAround.LoadLevelWorkaround("LeaderBoard");
			break;
		}
	}

    void OnGUI(){

		GUI.depth = 0;

		GUI.DrawTexture (bSpotRect, BSpot);

		GUI.DrawTexture (shufflebowlingRect, shuffleLogo);


//		if (GUI.Button (podiumRect, "", podiumStyle)) {
//			StartCoroutine(PlayAudio(buttonClick, "LeaderBoard"));
//		}

		if (GUI.Button (playForFunRect,"", PlayForFunStyle)) {
			StartCoroutine(PlayAudio(mainButtons, "PlayForFun"));
			Properties.GameType = Properties.Modes.PLAYFORFUN;
			Properties.ChallengeMode = "";
		}

		if (GUI.Button (playForCashRect, "", PlayForMoneyStyle)) {

			Properties.GameType = Properties.Modes.PLAYFORMONEY;

			StartCoroutine(PlayAudio(mainButtons, "PlayForCash"));

//			if (!Properties.IsLoggedIn)
//			{
//				Application.OpenURL(BasicHandShake.loginUrl + "?successUrl=" + BasicHandShake.thisPage + "&apiKey=" + BasicHandShake.apiKey);
//				Debug.Log("testing this: " + (BasicHandShake.loginUrl + "?successUrl=" + BasicHandShake.thisPage + "&apiKey=" + BasicHandShake.apiKey));
//			}
//			else 
//			{
//				WorkAround.LoadLevelWorkaround("ChallengesScene");
//			}
		}

//		GUI.DrawTexture (playForFunRect, playForFun);
//		GUI.DrawTexture (playForCashRect, playForCash);

//		if (GUI.Button (optionsRect, "",optionsStyle)) {
//			StartCoroutine(PlayAudio(buttonClick, "Settings"));
//		}
//		
//		if (GUI.Button (helpRect, "", helpStyle)) {
//			StartCoroutine(PlayAudio(buttonClick, "Help"));
//		}
    }
}
