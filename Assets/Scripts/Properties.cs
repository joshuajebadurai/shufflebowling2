﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Properties {

	public enum PlayForFunMode{
		ARCADE,
		CLASSIC
	};

	public static PlayForFunMode PlayMode;

	public enum Modes{
		PLAYFORFUN,
		PLAYFORMONEY
	};

	public static int challengeNumber = 0;
	public static bool challengeDirection = false;

	public static int challengeAttempts = 0;
	public static int currentAttempt = 1;

	public static bool isChallengeFinished =  false;

	public static Modes GameType;

	public static bool isPopUpShown = false;

	public static List<int>arcadeScoresList = new List<int>();
	public static List<int>classicScoresList = new List<int>();

	public static int LeaderBoardSize = 10;

	public static string BaseUrl="https://sb.oddz.com/api/";
	public static string Version="v1.1";

	public static string WonAmount = "";

	public static string ChallengeMode;

	public static string ParentScene;
	public static string PreviousScene;
	public static string CurrentScene;

	public static float SelectedDenomination;
	public static double TotalWinnings = 0;
	//public static float GamePlayBalance;
	public static double GamePlayBalance = 0;
	public static double CashBalance = 0;
	public static string SelectedChallenge;
	public static string CurrentScore;
	public static bool isScoreHightest = false;
	public static string HighestScore;
	//public static ChallengesCollection challengesCollection;
	
	//number of more tickets needed to unlock the challenge
	public static int MoreTickets;
	public static int Tickets;
	public static int TotalTickets;

	public static bool S_CantakeInputs = false;
	public static bool S_GoingtoPlay = false;
	public static string GameWonState;
	public static int TotalFrames;
	public static string SessionKey;
	public static bool IsLoggedIn = false;
	public static bool DeleteSession=false;
	public static string AcessKey;
	
	public static float sfxVolume = 0.6f;
	public static float voiceOverVolume = 0.6f; 
	public static float musicVolume = 0.3f;
	
	public static bool IsLogoTransitionsAllowed = false;
	
	public static List<int> blink1List = new List<int>(){
		1, 4, 6 
	};
	
	public static List<int[]> blink2List = new List<int[]>(){
		new int[] {2,3},
		new int[] {2,4},
		new int[] {3,6},
		new int[] {7,10}
	};
	
	public static List<int[]> blink3List = new List<int[]>(){
		new int[] { 1, 2, 3 },
		new int[] { 1, 2, 4 },
		new int[] { 1, 3, 6 },
		new int[] { 1, 7, 10 }
	};
	
	public enum RequestType
	{
		none,
		Session,
		Account,
		GamePlay,
		GamePlay_Events,
		Play,
		Balances,
		WithDraw,
		DeleteSession,
		CancelTokens,
	};
	
	public static string[] ChallengesList = new string[]{
		"Take out the headpin in one shot!",
		"Pick up the 2-7 split to win!",
		"Pick up the 1-5 pin split to win!",	
		"Pick up the 7-10 pin split to win!",
		"Pick up the 4-6-7-10 pin split to win!",
		"Pick up the 2-4-5-8 pin split to win!",
		"Pick up the 2-7-10 pin split to win!",
		"Get two strikes in a row!",
		"Get three strikes in a row!",
		"Get 12 Strikes in a row!"
	};
	
	
	public static Dictionary<string, string> challengeDictionary = new Dictionary<string, string> (){
		{ChallengesList[0], "The Sniper"},
		{ChallengesList[1], "Baby Split"},
		{ChallengesList[2], "Sleeper"},
		{ChallengesList[3], "Bedposts"}, 
		{ChallengesList[4], "Golden Gate"},
		{ChallengesList[5], "The Bucket"},
		{ChallengesList[6], "Christmas Tree"},
		{ChallengesList[7], "Double Double"},
		{ChallengesList[8], "Wild Turkeys"},
		{ChallengesList[9], "Perfect Game"}
	};

	//Dictionary contains challengeName,[win,lose,unlock value] 
	public static Dictionary<string,ChallengeProperties> winLoseDictionary = new Dictionary<string, ChallengeProperties>(){
		{challengeDictionary[ChallengesList[0]] , new ChallengeProperties(0, challengeDictionary[ChallengesList[0]], 100, 100,    0, false, 3, new string[]{"0"})},
		{challengeDictionary[ChallengesList[1]] , new ChallengeProperties(1, challengeDictionary[ChallengesList[1]], 100, 100,  250, false,  2,	new string[]{"1","6"})},
		{challengeDictionary[ChallengesList[2]] , new ChallengeProperties(2, challengeDictionary[ChallengesList[2]], 100, 150,  350, false,  2,	new string[]{"0","4"})},
		{challengeDictionary[ChallengesList[3]] , new ChallengeProperties(3, challengeDictionary[ChallengesList[3]], 100, 200,  550, false,  3, new string[]{"6","9"})},
		{challengeDictionary[ChallengesList[4]] , new ChallengeProperties(4, challengeDictionary[ChallengesList[4]], 125, 200,  750, false,  4, new string[]{"3","5","6","9"})},
		{challengeDictionary[ChallengesList[5]] , new ChallengeProperties(5, challengeDictionary[ChallengesList[5]], 135, 200, 1000, false,  4, new string[]{"1","3","4","7"})},
		{challengeDictionary[ChallengesList[6]] , new ChallengeProperties(6, challengeDictionary[ChallengesList[6]], 150, 250, 1250, false,  3, new string[]{"1","6","9"})},
		{challengeDictionary[ChallengesList[7]] , new ChallengeProperties(7, challengeDictionary[ChallengesList[7]], 175, 275, 1500, false,  2, new string[]{"0","1","2","3","4","5","6","7","8","9"})},
		{challengeDictionary[ChallengesList[8]] , new ChallengeProperties(8, challengeDictionary[ChallengesList[8]], 175, 275, 2000, false,  3, new string[]{"0","1","2","3","4","5","6","7","8","9"})},
		{challengeDictionary[ChallengesList[9]] , new ChallengeProperties(9, challengeDictionary[ChallengesList[9]], 175, 275, 3000, false,  5, new string[]{"0","1","2","3","4","5","6","7","8","9"})}
	};
}
