using System;
using UnityEngine;
using System.Collections;

public class AudioManager:MonoBehaviour
{
	public enum AudioType
	{
		Click,
		VoiceOver,
		BGAudio
	}

	private static AudioManager instance;

	private float audioVolume;

	void Awake() 
	{
		if(instance == null)
		{
			//If I am the first instance, make me the Singleton
			instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != instance)
				Destroy(this.gameObject);
		}
	}

	public static AudioSource AudioSource; 

	public static AudioManager Instance
	{
		get 
		{
			if (instance == null)
			{
				instance = GameObject.FindObjectOfType<AudioManager>();
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(instance.gameObject);
			}
			return instance;
		}
	}

	public IEnumerator PlayAudio(AudioClip audioClip, string buttonName, AudioType audioType){

		switch (audioType) {

		case AudioType.Click:

			audio.volume = Properties.musicVolume;

			audio.PlayOneShot (audioClip);

			yield return new WaitForSeconds (audioClip.length);

			switch (buttonName)
			{
			case "Help":
				WorkAround.LoadLevelWorkaround("Help");
				break;
			case "Options":
				WorkAround.LoadLevelWorkaround("Options");
				break;
			case "Back":
				//WorkAround.LoadLevelWorkaround(Properties.ParentScene);
				break;
			case "Win":
				
				break;
			case "Lose":
				
				break;
			case "Ok":
				
				break;
			case "Cancel":
				
				break;
			case "Podium":
				WorkAround.LoadLevelWorkaround("LeaderBoard");
				break;
			case "Settings":
				WorkAround.LoadLevelWorkaround("SettingsScene");
				break;
			case "LeaderBoard":
				WorkAround.LoadLevelWorkaround("LeaderBoard");
				break;
			case "PlayForCash":
				break;
			case "PlayForFun":
				WorkAround.LoadLevelWorkaround("MainMenu");
				break;
			case "PlayAnotherChallenge":
				WorkAround.LoadLevelWorkaround("ChallengesScene");
				break;
			case "PlayAgain":
				Properties.GameType = Properties.Modes.PLAYFORFUN;
				WorkAround.LoadLevelWorkaround("MainGame");
				break;
			case "MainMenu":
			WorkAround.LoadLevelWorkaround("ModesScene");
				break;
			case "LogOut":
				break;
			case "Quit":
				break;
			case "UnLockChallenge":
				break;
			case "Right":
				break;
			case "Left":
				break;
			case "Play":
				break;
			case "Arcade":
				WorkAround.LoadLevelWorkaround("MainGame");
				break;
			case "Classic":
				WorkAround.LoadLevelWorkaround("MainGame");
				break;
			}
			break;
		case AudioType.BGAudio:

//			audio.volume = Properties.musicVolume;
			audio.Play();
//			audio.loop = true;
			yield return null;

			break;

		case AudioType.VoiceOver:

			audio.volume = Properties.voiceOverVolume;
			audio.Play();
			yield return new WaitForSeconds (audioClip.length);

			break;
		}
	}

}

