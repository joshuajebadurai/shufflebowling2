﻿using UnityEngine;
using System.Collections;

public class Exit : MonoBehaviour {

	public AudioClip menuOn;
	public AudioClip menuOff;
	public AudioClip buttonClip;

	public Texture2D yes;
	public Texture2D no;

	public Rect yesRect;
	public Rect noRect;

	public GUIStyle style;
	public GUIStyle yesStyle;
	public GUIStyle noStyle;

	// Use this for initialization
	void Start () {

		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(menuOn);
//		Properties.musicVolume = 0.3f;
//		PlayerPrefs.SetFloat("MusicVolume",Properties.musicVolume);
//		PlayerPrefs.Save();
//		AudioManager.AudioSource.audio.volume = Properties.musicVolume;

		yesRect = new Rect (320,300,yes.width,yes.height);	
		noRect = new Rect (500,300,no.width,no.height);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator PlayAudio(AudioClip audioClip, string buttonName){
		audio.volume = Properties.sfxVolume;
		audio.PlayOneShot(audioClip);
		yield return new WaitForSeconds (audioClip.length );
		
		audio.PlayOneShot(menuOff);
		yield return new WaitForSeconds (menuOff.length );
		
		switch (buttonName) {
		case "Yes":
			if(Properties.GameType == Properties.Modes.PLAYFORFUN)
				WorkAround.LoadLevelWorkaround("ModesScene");
			else
				WorkAround.LoadLevelWorkaround("ChallengesScene");
			break;
		case "No":
			WorkAround.LoadLevelWorkaround(Properties.PreviousScene);
			break;
		case "Left":
			break;
			
		}
	}

	void OnGUI(){
		if(GUI.Button(yesRect, "", yesStyle)){
			StartCoroutine(PlayAudio(buttonClip, "Yes"));
		}
		if(GUI.Button(noRect, "", noStyle)){
			StartCoroutine(PlayAudio(buttonClip, "No"));
		}
	}
}
